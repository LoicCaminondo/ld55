using FMODUnity;
using Sirenix.OdinInspector;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Zenject;

public class OnClickSendSignalBackgroundMusicType : MonoBehaviour
{
    [Inject(Id = "MonsterStat")] private InfoBubble _statBubble;
    [Inject] SignalBus _signalBus;

    [SerializeField] private BackgroundMusicType _ID;
    [SerializeField, TextArea(4, 10)] string Description;
    [SerializeField] private EventReference _bubble;
    [SerializeField] private SpriteRenderer _highlight;

    private bool _isEnabled = true;

    private void Awake()
    {
        _signalBus.Subscribe<SStartingBrewing>(Disable);
        _signalBus.Subscribe<STaskLaunched>(Enable);
        _signalBus.Subscribe<SBackgroundChangeMusicSignal>(SelectUnselect);
        _signalBus.Subscribe<SMonsterBrewed>(UnselectAll);
    }

    private void SelectUnselect(SBackgroundChangeMusicSignal signal)
    {
        if (signal.Id == _ID)
        {
            _highlight.gameObject.SetActive(true);
        }
        else
        {
            _highlight.gameObject.SetActive(false);
        }
    }
    
    private void UnselectAll()
    {
        _highlight.gameObject.SetActive(false);
    }

    public void Enable()
    {
        _isEnabled = true;
    }

    public void Disable()
    {
        _isEnabled = false;
    }

    public void OnMouseEnter()
    {
        _statBubble.TextObj.text = Description;
        _statBubble.OnMouseOver();
    }

    public void OnMouseExit()
    {
        _statBubble.OnMouseExit();
    }

    public void OnMouseDown()
    {
        RuntimeManager.PlayOneShot(_bubble);
        if (_isEnabled)
            SendSignal();
    }

    private void SendSignal()
    {
        _signalBus.Fire(new SBackgroundChangeMusicSignal(_ID));
    }

}