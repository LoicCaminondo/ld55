using System.Collections;
using UnityEngine;

public class SquashOnClick : MonoBehaviour
{
    [SerializeField] private Transform GraphicsObject;
    [SerializeField] private float xSqueeze;
    [SerializeField] private float ySqueeze;
    [SerializeField] private float duration;
    [SerializeField] private bool isInstant;
    [SerializeField] private float delay;

    private IEnumerator OnMouseDown()
    {
        yield return Squeeze();
    }

    private IEnumerator Squeeze()
    {
        if (delay > 0.0f)
            yield return new WaitForSeconds(delay);

        float lookDirection = Mathf.Sign(GraphicsObject.localScale.x);
        
        Vector3 originalSize = new Vector3(lookDirection, 1.0f, 1.0f);
        Vector3 newSize = new Vector3(xSqueeze * lookDirection, ySqueeze, originalSize.z);

        float t = 0f;
        if (isInstant == false)
        {
            while (t <= 1.0)
            {
                t += Time.deltaTime / duration;
                GraphicsObject.localScale = Vector3.Lerp(originalSize, newSize, t);
                yield return null;
            }
            t = 0f;
        }
        while (t <= 1.0)
        {
            t += Time.deltaTime / duration;
            GraphicsObject.localScale = Vector3.Lerp(newSize, originalSize, t);
            yield return null;
        }
    }
}
