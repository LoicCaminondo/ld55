using FMODUnity;
using System.Collections;
using Unity.VisualScripting;
using UnityEngine;
using Zenject;

public class ChangeTimeOnClick : MonoBehaviour
{
    [Inject] DayNightManager _dayNightManager;
    [Inject(Id = "MonsterStat")] private InfoBubble _statBubble;


    [SerializeField] private GameObject nightFilter;
    [SerializeField] private float duration;
    
    [SerializeField] private Sprite daySprite;
    [SerializeField] private Sprite nightSprite;
    [SerializeField, TextArea(4, 10)] private string description;

    [SerializeField] private EventReference _bubble;


    private bool isNightTime = false;
    private bool isFading = false;
    

    private SpriteRenderer spriteRenderer;

    private void Start()
    {
        spriteRenderer = nightFilter.GetComponent<SpriteRenderer>();
    }

    private IEnumerator OnMouseDown()
    {
        if (isFading)
            yield break;
        RuntimeManager.PlayOneShot(_bubble);
        yield return ChangeTime();
    }

    public void OnMouseEnter()
    {
        _statBubble.TextObj.text = description;
        _statBubble.OnMouseOver();
    }

    public void OnMouseExit()
    {
        _statBubble.OnMouseExit();
    }

    private IEnumerator ChangeTime()
    {
        isNightTime = !isNightTime;

        transform.GetChild(0).GetComponent<SpriteRenderer>().sprite = isNightTime ? nightSprite : daySprite;
        _dayNightManager.SwitchDayNight();

        if (isNightTime)
            yield return FadeIn();
        else
            yield return FadeOut();
    }

    private IEnumerator FadeIn()
    {
        float fade = 0.0f;
        float timer = duration;
        float increment = 0.8f / duration;
        Color color = spriteRenderer.color;
     
        isFading = true;
        while (timer > 0.0f)
        {
            color.a = fade;
            spriteRenderer.color = color;
            
            float delta = Time.deltaTime;
            fade += delta * increment;
            timer -= delta;
            
            yield return null;
        }
        isFading = false;
        
        color.a = 0.8f;
        spriteRenderer.color = color;
    }

    private IEnumerator FadeOut()
    {
        float fade = 0.8f;
        float timer = duration;
        float increment = 0.8f / duration;
        Color color = spriteRenderer.color;
        
        isFading = true;
        while (timer > 0.0f)
        {
            color.a = fade;
            spriteRenderer.color = color;
            
            float delta = Time.deltaTime;
            fade -= delta * increment;
            timer -= delta;
            
            yield return null;
        }
        isFading = false;
        
        color.a = 0.0f;
        spriteRenderer.color = color;
    }
}