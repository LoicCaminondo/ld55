using UnityEngine;

public class DetectItem : MonoBehaviour
{
    private void OnTriggerEnter2D(Collider2D other)
    {
        if (other.CompareTag("Item"))
            other.transform.position = new Vector3(0.0f, -15f, 0.0f);
    }
}
