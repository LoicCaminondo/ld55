using System;
using System.Collections.Generic;
using Sirenix.OdinInspector;
using UnityEngine;
using UnityEngine.Events;
using Zenject;

/*
 *
 * - First tab :
 *
 * - lock / unlock hidden / unlock summoned
 * 
 * - Info sur les premade monster
 * - One page by monster
 * 
 * - name of the monster
 * - hint
 * - visual display (once summoned)
 * - composition / ingredients (once summoned)
 * - stats du monster (once summoned)
 *
 * - pages unlock with progress
 * -
 * -
 *
 * 
 */



public class Encyclopedia : MonoBehaviour
{
    [Serializable]
    public class Page
    {
        public enum UnlockState
        {
            Locked,
            Hidden,
            Summoned
        }

        [ShowInInspector] public UnlockState State { get; private set; }
        [field: SerializeField] public MonsterRecipee MonsterRecipee { get; private set; }
        [field: SerializeField] public GameObject PageGameObject { get; private set; }

        public void Summoned()
        {
            State = UnlockState.Summoned;
        }

        public void DisplayPageContent()
        {
            PageGameObject.transform.GetChild(0).GetChild(0).gameObject.SetActive(State == UnlockState.Summoned);
            PageGameObject.transform.GetChild(0).GetChild(1).gameObject.SetActive(State != UnlockState.Summoned);
            PageGameObject.transform.GetChild(1).GetChild(0).gameObject.SetActive(State == UnlockState.Summoned);
            PageGameObject.transform.GetChild(1).GetChild(1).gameObject.SetActive(State != UnlockState.Summoned);
            PageGameObject.transform.GetChild(3).gameObject.SetActive(State == UnlockState.Summoned);
        }
    }
    [Inject] private SignalBus _signalBus;
    [SerializeField] private List<Page> pages;

    public static UnityEvent OnOpenEncyclopedia = new UnityEvent();
    public static UnityEvent<int> OnRequestChangePage = new UnityEvent<int>();
    public static UnityEvent OnCloseEncyclopedia = new UnityEvent();

    private int currentPage = 0;

    void Start()
    {
        OnOpenEncyclopedia.AddListener(Open);
        OnRequestChangePage.AddListener(ChangePage);
        _signalBus.Subscribe<SRecipeBrewed>(OnPerfectRecipeBrewed);
    }

    private void OnPerfectRecipeBrewed(SRecipeBrewed signal)
    {
        foreach (var page in pages)
        {
            if (page.MonsterRecipee == signal.MonsterRecipee && page.State != Page.UnlockState.Summoned)
            {
                page.Summoned();
                _signalBus.Fire(new SNewRecipeUnlockSignal());
            }
        }
    }

    private void ChangePage(int direction)
    {
        currentPage += direction;
        currentPage = Math.Clamp(direction, 0, 1);
        LoadPage(currentPage);
    }

    private void Open()
    {
        if (pages == null || pages.Count < 1)
            return;

        LoadPage(currentPage);
    }

    private void LoadPage(int index)
    {
        for (int i = 0; i < pages.Count; i++)
        {
            pages[i].PageGameObject.SetActive(i == index);
        }

        pages[index].DisplayPageContent();
    }
}

public class SNewRecipeUnlockSignal
{
}