using FMODUnity;
using System.Collections;
using UnityEngine;

public class ShakeOnItemPickup : MonoBehaviour
{
    [SerializeField] private float waitTime;
    [SerializeField] private float power;
    [SerializeField] private float speed;

    private bool isShaking = false;
    
    private void Start()
    {
        PickupItem.OnPickupItem.AddListener(() => StartCoroutine(Shake()));
        PickupItem.OnDropItem.AddListener(() => isShaking = false);
    }

    private IEnumerator Shake()
    {
        isShaking = true;
        float upAngle = Tools.DirectionToDegree(Vector2.right);

        bool positive = true;
        while (isShaking)
        {
            float angle = upAngle + (power * (positive ? 1.0f : -1.0f));

            yield return Smooth(angle);
            
            positive = !positive;
        }

        transform.rotation = Tools.DegreeToVector2(upAngle).ToRotation();
    }

    
    private IEnumerator Smooth(float angle)
    {
        Quaternion target = Tools.DegreeToVector2(angle).ToRotation();

        float timer = 0.0f;
        while (timer < waitTime)
        {
            transform.rotation = Quaternion.Lerp(transform.rotation, target, Time.deltaTime * speed);
            yield return null;
            timer += Time.deltaTime;
        }
    }
}
