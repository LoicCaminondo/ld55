using FMODUnity;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CloseEncyclopedia : MonoBehaviour
{
    [SerializeField] private GameObject encyclopediaObject;
    [SerializeField] private GameObject buttonToActivate;
    [SerializeField] EventReference _sound;

    private Coroutine routine;
    
    private void OnMouseDown()
    {
        RuntimeManager.PlayOneShot(_sound);
        if (routine != null)
        {
            StopAllCoroutines();
        }

        routine = StartCoroutine(CloseRoutine());
    }

    private IEnumerator CloseRoutine()
    {
        yield return new WaitForSeconds(0.3f);
        
        encyclopediaObject.SetActive(false);
        buttonToActivate.SetActive(true);
    }
}
