using System.Linq;
using UnityEngine;
using UnityEngine.Events;
using Zenject;

public class PickupItem : MonoBehaviour
{
    [Inject] readonly SignalBus _signalBus;
    [Inject] readonly SimpleTaskManager _simpleTaskManager;

    [SerializeField] private LayerMask layerMask;

    public static UnityEvent OnDropItem = new UnityEvent();
    public static UnityEvent OnPickupItem = new UnityEvent();

    private Camera mainCamera;
    private GameObject currentItem = null;

    private Vector2 offset = Vector2.zero;

    void Start()
    {
        mainCamera = Camera.main;
    }

    void Update()
    {
        if (_simpleTaskManager?.IsTaskIsRunning == false)
            return;

        if (Input.GetMouseButtonDown(0))
        {
            if (currentItem == null)
                PickupItemWithMouse();
            else
                DropItem();
        }

        if (currentItem != null)
            currentItem.transform.position = ComputeMousePosition() + offset;
    }

    private void DropItem()
    {
        currentItem.GetComponent<Rigidbody2D>().bodyType = RigidbodyType2D.Dynamic;
        currentItem.transform.GetChild(0).GetComponent<SpriteRenderer>().sortingOrder = 5;
        OnDropItem?.Invoke();
        currentItem = null;
    }

    private Vector2 ComputeMousePosition()
    {
        return mainCamera.ScreenToWorldPoint(Input.mousePosition);
    }

    private void PickupItemWithMouse()
    {
        Vector2 position = ComputeMousePosition();

        Collider2D[] colliders = Physics2D.OverlapPointAll(position, layerMask);

        bool hitItem = colliders.Any(c => c.CompareTag("Item"));

        if (hitItem)
        {
            currentItem = colliders.First(c => c.CompareTag("Item")).gameObject;
            currentItem.GetComponent<Rigidbody2D>().bodyType = RigidbodyType2D.Static;
            if (currentItem.TryGetComponent(out Ingredient bodyPart))
            {
                _signalBus.AbstractFire<SIngredPickup>();
            }

            var spriteRenderer = currentItem.transform.GetChild(0).GetComponent<SpriteRenderer>();
            spriteRenderer.sortingLayerName = "Tools";
            spriteRenderer.sortingOrder = 7;
            OnPickupItem?.Invoke();
            offset = (Vector2)currentItem.transform.position - position;
            offset += Vector2.up * 0.15f;
        }
    }
}


