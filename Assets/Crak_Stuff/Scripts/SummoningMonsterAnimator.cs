using Cysharp.Threading.Tasks;
using FMOD.Studio;
using FMODUnity;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using Zenject;

public class SummoningMonsterAnimator : MonoBehaviour
{
    [Inject(Id = "MonsterAnchor")] Transform monster;
    [Inject] MonsterMixer _monsterMixer;
    [Inject] SignalBus _signalBus;

    [SerializeField] EventReference _boilSummoning;
    public static UnityEvent OnStartSummon = new UnityEvent();

    private void Start()
    {
        OnStartSummon.AddListener(PlaySpawningAnimation);
    }

    public void SpawnMonster()
    {
        _signalBus.Fire(new SFadeOut());
        _monsterMixer.Brew();
    }


    //1 to 4 LIgth
    //4 to 7 Medium
    // 7 to 10 Large
    private void PlaySpawningAnimation()
    {
        _signalBus.AbstractFire(new SStartingBrewing());

        List<Stat> currentStats = _monsterMixer.GetCurrentMonsterStats();
        int masIndex = currentStats.FindIndex(x => x.Type == StatType.Mass);
        if (masIndex >= 0)
        {
            float massValue = Mathf.Clamp(((float)currentStats[masIndex].Value) / 3f, 1f, 10f);
            EventInstance boilInstance = RuntimeManager.CreateInstance(_boilSummoning);
            boilInstance.setParameterByName("Mass", massValue);
            boilInstance.start();
            boilInstance.release();

        }

        GetComponent<Animator>().Play("Summon");
    }
}

public class SStartingBrewing
{
}
