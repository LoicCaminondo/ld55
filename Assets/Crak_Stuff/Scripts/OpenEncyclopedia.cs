using FMODUnity;
using System.Collections;
using UnityEngine;

public class OpenEncyclopedia : MonoBehaviour
{
    [SerializeField] private GameObject encyclopediaObject;
    [SerializeField] EventReference _sound;

    private Coroutine routine;
    
    private void OnMouseDown()
    {
        RuntimeManager.PlayOneShot(_sound);
        if (routine != null)
        {
            StopAllCoroutines();
        }

        routine = StartCoroutine(OpenRoutine());
    }
    
    private IEnumerator OpenRoutine()
    {
        yield return new WaitForSeconds(0.3f);
        
        Encyclopedia.OnOpenEncyclopedia?.Invoke();
        
        encyclopediaObject.SetActive(true);
        gameObject.SetActive(false);
    }
}
