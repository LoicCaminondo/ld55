using System.Collections;
using UnityEngine;
using Zenject;

public class ShakeOnNewRecipeUnlock : MonoBehaviour
{
    [Inject] SignalBus _signalBus;
    [SerializeField] private float waitTime;
    [SerializeField] private float power;
    [SerializeField] private float speed;

    private bool isShaking = false;
    private Quaternion originalRotation;

    private void OnDisable()
    {
        isShaking = false;
        transform.rotation = originalRotation;
    }

    private void Start()
    {
        originalRotation = transform.rotation;
        _signalBus.Subscribe<SNewRecipeUnlockSignal>(() => StartCoroutine(Shake()));
    }

    private IEnumerator Shake()
    {
        Debug.Log("Shake");
        isShaking = true;
        float upAngle = Tools.DirectionToDegree(Vector2.right);

        bool positive = true;
        while (isShaking)
        {
            float angle = upAngle + (power * (positive ? 1.0f : -1.0f));

            yield return Smooth(angle);

            positive = !positive;
        }
    }

    private IEnumerator Smooth(float angle)
    {
        Quaternion target = Tools.DegreeToVector2(angle).ToRotation();

        float timer = 0.0f;
        while (timer < waitTime)
        {
            transform.rotation = Quaternion.Lerp(transform.rotation, target, Time.deltaTime * speed);
            yield return null;
            timer += Time.deltaTime;
        }
    }
}
