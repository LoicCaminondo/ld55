using System.Collections;
using UnityEngine;
using Zenject;

public class Respawnable : MonoBehaviour
{
    [Inject] SignalBus _signalBus;

    [SerializeField] private SpriteRenderer spriteRenderer;
    private Vector2 startingPosition;

    private void Start()
    {
        startingPosition = transform.position;
    }

    void Update()
    {
        if (transform.position.y <= -10.0f)
        {
            _signalBus.AbstractFire<SIngredBin>();
            Respawn();
        }
    }

    public void Respawn()
    {
        transform.position = startingPosition;
        GetComponent<Rigidbody2D>().bodyType = RigidbodyType2D.Static;
        if (TryGetComponent(out Ingredient ingredient))
        {
            ingredient.OnRespawn();
        }
        spriteRenderer.sortingLayerName = "Background";
        transform.rotation = Quaternion.identity;
        StartCoroutine(Tools.Fade(spriteRenderer, 1.5f, true));
        StartCoroutine(GrowBack());
    }

    private Vector3 growVelocity;
    private IEnumerator GrowBack()
    {
        float timer = 0.5f;
        Vector3 savedSize = transform.localScale;
        transform.localScale = Vector3.one * 0.5f;

        while (timer > 0.0f)
        {
            transform.localScale = Vector3.SmoothDamp(transform.localScale, savedSize, ref growVelocity, timer);
            yield return null;
            timer -= Time.deltaTime;
        }

        transform.localScale = savedSize;
    }
}
