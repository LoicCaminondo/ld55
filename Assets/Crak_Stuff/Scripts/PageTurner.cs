using FMODUnity;
using UnityEngine;

public class PageTurner : MonoBehaviour
{
    [SerializeField] private bool towardsLeft;
    [SerializeField] EventReference _sound;


    private void OnMouseDown()
    {
        RuntimeManager.PlayOneShot(_sound);
        Encyclopedia.OnRequestChangePage?.Invoke(towardsLeft ? -1 : 1);
    }
}
