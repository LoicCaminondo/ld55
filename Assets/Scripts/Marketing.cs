using System.Collections;
using System.Collections.Generic;
using Cysharp.Threading.Tasks;
using UnityEngine;

public class Marketing : MonoBehaviour
{
    async UniTaskVoid Start()
    {
        await UniTask.DelayFrame(9);
        GetComponent<Animator>().Play("Marketing");
    }
}
