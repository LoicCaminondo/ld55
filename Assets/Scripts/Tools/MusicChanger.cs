using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Zenject;

public class MusicChanger : MonoBehaviour
{
    [Inject] private readonly MusicManager _musicManager;

    void Start()
    {
        _musicManager.SwitchToGameMusic().Forget();
    }
}
