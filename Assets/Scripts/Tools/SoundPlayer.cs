using Sirenix.OdinInspector;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Zenject;

public class SoundPlayer : MonoBehaviour
{
    [Inject] SignalBus _signalBus;

    [Button("Play event my friend")]
    public void PlaySound()
    {
        Debug.Log("Fire");
        _signalBus.AbstractFire<SGameOver>();
    }
}
