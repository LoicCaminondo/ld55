using System.Collections;
using System.Collections.Generic;
using Cysharp.Threading.Tasks;
using UnityEngine;
using Zenject;

public class SignalWaiter<T>
{
    private SignalBus _signalBus;
    private bool _isSignaled = false;

    public SignalWaiter(SignalBus signalBus)
    {
        _signalBus = signalBus;
        _isSignaled = false;
        _signalBus.Subscribe<T>(OnSignal);
    }

    ~SignalWaiter()
    {
        _signalBus.Unsubscribe<T>(OnSignal);
    }

    public void OnSignal()
    {
        _isSignaled = true;
    }

    public async UniTask Wait()
    {
        await UniTask.WaitUntil(() => _isSignaled);
    }

    public void Reset()
    {
        _isSignaled = false;
    }

    public class Factory : PlaceholderFactory<SignalWaiter<T>> { }
}
