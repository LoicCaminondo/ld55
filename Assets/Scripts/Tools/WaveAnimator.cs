using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WaveAnimator : MonoBehaviour
{
    [SerializeField] private float _wavePower = 0.1f;
    [SerializeField] private float _waveSpeed = 1f;

    void Update()
    {
        Vector3 pos = transform.localPosition;
        pos.y = Mathf.Sin(Time.time * _waveSpeed) * _wavePower;
        transform.localPosition = pos;
    }
}
