using Sirenix.OdinInspector;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TestBasicTask : MonoBehaviour
{
    public GameTask toTest;

    public List<Stat> stats;

    [Button("Summon")]
    public void SummonTask()
    {
        Debug.Log(string.Join("; ", toTest.Descriptions));
    }

    [Button("Solve")]
    public void SolveTask()
    {
        toTest.TryToSolve(stats);
    }
}
