using System.Collections;
using Cysharp.Threading.Tasks;
using System.Collections.Generic;
using Sirenix.OdinInspector;
using Signals;
using UnityEngine;
using Zenject;
using System;

public class GameSignalsMockup : MonoBehaviour
{
    [SerializeField] private float _loadingDelay = 1f;
    [Inject] private readonly SignalBus _signalBus;

    private int _score = 0;
    private int _combo = 0;
    private bool Initialized => _signalBus != null;

    private async void Start()
    {
        await UniTask.Delay(TimeSpan.FromSeconds(_loadingDelay));
        _signalBus.Fire(new SLoadingDone());
    }

    private void FireScoreUpdate(int newScore)
    {
        //_signalBus.Fire(new ScoreUpdate(newScore));
    }

    private void FireComboUpdate(int newCombo)
    {
        //_signalBus.Fire(new ComboUpdate(newCombo));
    }


    private void FireGameOver()
    {
        //_signalBus.Fire(new GameOver());
    }

    private void FireFinalScore(int score)
    {
        //_signalBus.Fire(new FinalScore(score));
    }

    [EnableIf("Initialized")]
    [Button("Add Points")]
    private void AddPoints(int points)
    {
        FireComboUpdate(_combo += 1);
        FireScoreUpdate(_score += points * _combo);
    }

    [EnableIf("Initialized")]
    [Button("Reset Combo")]
    private void ResetCombo()
    {
        _combo = 0;
        FireComboUpdate(_combo);
    }

    [EnableIf("Initialized")]
    [Button("End Game")]
    private void EndGame()
    {
        FireGameOver();
        FireFinalScore(_score);
    }

    [EnableIf("Initialized")]
    [Button("Launch Scenario")]
    private async UniTaskVoid LaunchScenario()
    {
        await UniTask.Delay(1000);
        AddPoints(1);
        await UniTask.Delay(1000);
        AddPoints(2);
        await UniTask.Delay(500);
        AddPoints(4);
        await UniTask.Delay(1000);
        AddPoints(8);
        await UniTask.Delay(100);
        AddPoints(1);
        await UniTask.Delay(100);
        AddPoints(2);
        await UniTask.Delay(100);
        AddPoints(4);
        await UniTask.Delay(3000);
        ResetCombo();
        await UniTask.Delay(1000);
        AddPoints(1);
        FireGameOver();
        FireComboUpdate(_combo += 1); // should not change anything
        FireScoreUpdate(_score + 1 * _combo); // should not change anything
        FireFinalScore(_score);
    }
}