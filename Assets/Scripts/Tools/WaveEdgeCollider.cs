using System.Collections;
using System.Collections.Generic;
using System.Linq;
using Cysharp.Threading.Tasks;
using UnityEngine;

[RequireComponent(typeof(EdgeCollider2D))]
public class WaveEdgeCollider : MonoBehaviour
{
    [SerializeField] AnimationCurve _wavePower;
    private EdgeCollider2D _edgeCollider;
    private List<Vector2> _points;
    private float _centerX;

    void Start()
    {
        _edgeCollider = GetComponent<EdgeCollider2D>();
        _points = new List<Vector2>(_edgeCollider.points);
        float minX = _points.Min(p => p.x);
        _centerX = (_points.Max(p => p.x) + minX) / 2;
    }

    void Update()
    {
        List<Vector2> newPoints = new List<Vector2>();
        // change the points to follow a sinus wave pattern
        for (int i = 0; i < _points.Count; i++)
        {
            newPoints.Add(new Vector2(_points[i].x, _points[i].y + (Mathf.Sin(Time.time + Mathf.SmoothStep(0, 1, Mathf.Abs(_points[i].x - _centerX)) * -1) * _wavePower.Evaluate(i / (float)_points.Count))));
        }
        _edgeCollider.points = newPoints.ToArray();
    }
}
