using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Zenject;

public class CustomUISignalInstaller : Installer<CustomUISignalInstaller>
{
    public override void InstallBindings()
    {
        Container.DeclareSignal<FSendUISignal.STriggerClickButtonSound>();
        Container.DeclareSignal<FSendUISignal.STriggerHoverButtonSound>();
        Container.DeclareSignal<FSendUISignal.STriggerPanelSound>();
        Container.DeclareSignal<FSendUISignal.STriggerSliderSound>();
    }
}
