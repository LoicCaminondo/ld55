using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using MoreMountains.Tools;
using MoreMountains.Feedbacks;
using Zenject;

[AddComponentMenu("")]
[FeedbackHelp("Allow to send a predetermined set of signals")]
[FeedbackPath("CustomUI/SendUISignals")]
public class FSendUISignal : MMF_Feedback
{

    readonly SignalBus _signalBus;

    /// a static bool used to disable all feedbacks of this type at once
    public static bool FeedbackTypeAuthorized = true;
    /// use this override to specify the duration of your feedback (don't hesitate to look at other feedbacks for reference)
    public override float FeedbackDuration { get { return 0f; } }
    /// pick a color here for your feedback's inspector
#if UNITY_EDITOR
    public override Color FeedbackColor { get { return MMFeedbacksInspectorColors.SoundsColor; } }
#endif


    [MMFInspectorGroup("Signal Settings", true, 12, true)]
    [Tooltip("The signal to send")]
    public SignalType signalType;

    protected override void CustomInitialization(MMF_Player owner)
    {
        base.CustomInitialization(owner);

        // your init code goes here
    }

    protected override void CustomPlayFeedback(Vector3 position, float feedbacksIntensity = 1.0f)
    {
        if (!Active || !FeedbackTypeAuthorized)
        {
            return;
        }

        switch (signalType)
        {
            case SignalType.SoundHover:
                SignalBus.Instance.Fire<STriggerHoverButtonSound>();
                break;
            case SignalType.SoundClick:
                SignalBus.Instance.Fire<STriggerClickButtonSound>();
                break;
            case SignalType.SoundPanel:
                SignalBus.Instance.Fire<STriggerPanelSound>();
                break;
            case SignalType.SoundSlider:
                SignalBus.Instance.Fire<STriggerSliderSound>();
                break;

        }
    }

    protected override void CustomStopFeedback(Vector3 position, float feedbacksIntensity = 1)
    {
        if (!FeedbackTypeAuthorized)
        {
            return;
        }
        // your stop code goes here
    }


    public enum SignalType
    {
        SoundHover,
        SoundClick,
        SoundPanel,
        SoundSlider
    }

    public class STriggerHoverButtonSound { }
    public class STriggerClickButtonSound { }
    public class STriggerPanelSound { }
    public class STriggerSliderSound { }
}
