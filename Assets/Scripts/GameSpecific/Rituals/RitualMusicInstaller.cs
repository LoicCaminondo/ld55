using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Zenject;

public class RitualMusicInstaller : MonoInstaller
{
    public override void InstallBindings()
    {


        Container.BindInterfacesAndSelfTo<RitualMusicManager>().AsSingle();
    }
}

public class RitualMusicManager : IInitializable
{
    readonly SignalBus _signalBus;

    private BackgroundMusicType _backgroundMusicType;

    public RitualMusicManager(SignalBus signalBus)
    {
        _signalBus = signalBus;
    }

    public void Initialize()
    {
        _signalBus.Subscribe<SBackgroundChangeMusicSignal>(ChangeBackgroundMusic);
    }

    private void ChangeBackgroundMusic(SBackgroundChangeMusicSignal signal)
    {
        _backgroundMusicType = signal.Id;
    }

    // Will hard code the effect on music on the state because
    //    1. Seriously? You are reading this? You are lame 
    //    2. I hate you
    //    3. I hate myself more
    //    4. There will be only 3 to 4 music dude chill

    public void ModifyStats(List<Stat> currentStats)
    {
        switch (_backgroundMusicType)
        {
            case BackgroundMusicType.Druid:
                currentStats.Add(new Stat(StatType.Druid, 1));
                break;
            case BackgroundMusicType.Sorcerer:
                if (currentStats.Exists(x => x.Type == StatType.Strength))
                {
                    currentStats.Find(x => x.Type == StatType.Strength).Mult(3);
                }
                if (currentStats.Exists(x => x.Type == StatType.Intel))
                {
                    currentStats.Find(x => x.Type == StatType.Intel).Mult(3);
                }
                break;
            case BackgroundMusicType.Arcane:
                    if (currentStats.Exists(x => x.Type == StatType.Intel))
                    {
                        if (currentStats.Find(x => x.Type == StatType.Intel).Value >= 4)
                        {
                            currentStats.Add(new Stat(StatType.Arcana, 1));
                        }
                    }
                break;
        }
    }

}

