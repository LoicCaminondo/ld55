using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class ExtraModif : MonoBehaviour
{
    public virtual void Modify(List<Stat> stats)
    {

    }
}
