using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Zenject;

public class DayNightInstaller : MonoInstaller
{
    public override void InstallBindings()
    {
        Container.BindInterfacesAndSelfTo<DayNightManager>().AsSingle();
    }
}

public class DayNightManager
{
    [Inject] MusicManager _musicMainTitle;

    public DayNight dayState { get; private set; } = DayNight.Day;

    public DayNightManager()
    {

    }

    public void SwitchDayNight()
    {
        switch (dayState)
        {
            case DayNight.Day:
                dayState = DayNight.Night;
                break;
            case DayNight.Night:
                dayState = DayNight.Day;
                break;
            default:
                Debug.LogWarning("It's okay, but the state of the day night manager should never be another thing than Day or Night");
                break;
        }
        _musicMainTitle.SwitchDayNight(dayState);
    }

    public void ModifyStats(List<Stat> currentStats)
    {
        switch (dayState)
        {
            case DayNight.Day:
                break;
            case DayNight.Night:
                int carnivorousIndex = currentStats.FindIndex(x => x.Type == StatType.Discretion);
                if (carnivorousIndex >= 0)
                    currentStats[carnivorousIndex].Mult(3);
                break;
            default:
                Debug.LogWarning("It's okay, but the state of the day night manager should never be another thing than Day or Night");
                break;
        }
    }
}
