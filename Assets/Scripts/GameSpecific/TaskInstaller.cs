
using System.Collections.Generic;
using System;
using UnityEngine;
using Zenject;
using UnityEngine.Playables;
using Cysharp.Threading.Tasks;
using TMPro;
using Febucci.UI;
using System.Linq;
using System.Collections;
using Sirenix.OdinInspector;
using ModestTree;

public class TaskInstaller : MonoInstaller
{
    public List<GameTask> tasks;
    public SimpleTaskManager.Settings settings;

    public override void InstallBindings()
    {
        Container.DeclareSignal<SimpleTaskManager.SGameLaunch>();
        Container.BindInterfacesAndSelfTo<SimpleTaskManager>().AsSingle();
        Container.BindInstance(tasks).WhenInjectedInto<SimpleTaskManager>();
        Container.BindInstance(settings).WhenInjectedInto<SimpleTaskManager>();
    }
}

public class SimpleTaskManager : IInitializable, IDisposable
{
    [Inject] readonly AsyncProcessor _asyncProcessor;
    [Inject] readonly SignalBus _signalBus;
    [Inject] readonly List<GameTask> _gameTasks;
    [Inject(Id = "enter")] readonly PlayableDirector _enterPlayableDirector;
    [Inject(Id = "exit")] readonly PlayableDirector _exitPlayableDirector;
    [Inject] readonly CloseOnClick _closeOnClick;
    [Inject] readonly VolumeManager _volumeManager;
    [Inject] readonly Settings _settings;
    [Inject] readonly MusicManager _musicMainTitle;
    [Inject(Id = "ClientAnchor")] readonly Transform _clientAnchor;
    [Inject(Id = "EndScreen")] readonly Transform _endScreen;

    [Inject(Id = "EnterMonster")] readonly PlayableDirector _enterMonsterDirector;
    [Inject(Id = "ExitMonster")] readonly PlayableDirector _exitMonsterDirector;


    [Inject(Id = "clientBubble")] readonly Bubble _clientBubble;
    [Inject(Id = "TaskBubble")] readonly InfoBubble _infoBubble;
    //[Inject(Id = "exit")] readonly PlayableDirector _exitPlayableDirector;

    [Inject(Id = "MonsterAnchor")] readonly Transform _monsterParent;

    private GameTask _currentTask;

    private GameObject _currentClient;
    private int _currentTaskIndex = 0;

    public bool IsTaskIsRunning { get; private set; }

    private UniTask TimeLineWrapper(PlayableDirector playableDirector)
    {
        var tcs = new UniTaskCompletionSource();
        playableDirector.stopped += (x) => tcs.TrySetResult();
        playableDirector.Play();
        return tcs.Task;
    }

    private async UniTask ShowDescription(List<string> descriptions)
    {
        foreach (var description in descriptions)
        {
            await _clientBubble.Display(description);
        }
        await UniTask.WaitUntil(() => Input.GetMouseButtonDown(0), cancellationToken: _clientBubble.GetCancellationTokenOnDestroy());
        _clientBubble.gameObject.SetActive(false);
    }


    public async UniTaskVoid LaunchTask(GameTask gameTask)
    {


        IsTaskIsRunning = false;
        _musicMainTitle.GoBackToDefault();
        // Ici !
        // Client de se changer
        _currentClient = GameObject.Instantiate(_settings.clients[UnityEngine.Random.Range(0, _settings.clients.Count() - 1)], _clientAnchor);
        _currentClient.transform.localPosition = Vector3.zero;
        await TimeLineWrapper(_enterPlayableDirector);

        // Ici bon endroit pour le premier son 

        await ShowDescription(gameTask.Descriptions);

        _currentTask = gameTask;
        IsTaskIsRunning = true;
        _signalBus.Subscribe<SMonsterBrewed>(OnMonsterBrewed);
        _infoBubble.TextObj.text = gameTask.Descriptions.Last();
        _infoBubble.Enable();
        //_monsterBubble.Enable();

        _signalBus.Fire(new STaskLaunched());
    }

    public void Dispose()
    {
        _signalBus.TryUnsubscribe<SMonsterBrewed>(OnMonsterBrewed);
    }

    [InlineProperty, System.Serializable]
    public class Settings
    {
        public List<GameObject> clients;
    }

    public void Initialize()
    {
        _signalBus.Subscribe<SGameLaunch>(LaunchTask);
        if (!_closeOnClick.gameObject.activeInHierarchy)
        {
            LaunchTask();
        }
    }

    public class SGameLaunch
    {

    }

    public void LaunchTask()
    {
        _signalBus.TryUnsubscribe<SGameLaunch>(LaunchTask);
        LaunchTask(_gameTasks[_currentTaskIndex]).Forget();
    }

    private void OnMonsterBrewed(SMonsterBrewed sMonsterBrewed)
    {
        OnMonsterBrewedAsync(sMonsterBrewed).Forget();
    }

    private async UniTaskVoid OnMonsterBrewedAsync(SMonsterBrewed sMonsterBrewed)
    {
        _signalBus.Unsubscribe<SMonsterBrewed>(OnMonsterBrewed);

        await UniTask.WaitForSeconds(1.5f);

        _signalBus.AbstractFire<SAlive>();

        await UniTask.WaitForSeconds(3f);

        EndingEvent endingEvents = _currentTask.TryToSolve(sMonsterBrewed.brewedMonster.MonsterStat);

        foreach (var statChecker in endingEvents.StatCheckers)
        {
            StatType testedStat = statChecker.statType;
            foreach (var modifier in sMonsterBrewed.brewedMonster.Modifiers)
            {
                _signalBus.Fire(new SUpdateHiddenStat() { modifier = modifier, statType = testedStat });
            }
        }

        if (endingEvents == null)
            Debug.LogError("Null event should not happen in task manager");

        _infoBubble.Disable();
        _signalBus.Fire(new STaskEnded() { isSuccess = endingEvents.IsSuccess });

        // Client de faire son bruit selon le succes

        await ShowDescription(endingEvents.Descriptions);
        await TimeLineWrapper(_exitPlayableDirector);
        GameObject.Destroy(_currentClient.gameObject);
        GameObject.Destroy(sMonsterBrewed.brewedMonster.gameObject);

        if (endingEvents.IsSuccess)
        {
            _gameTasks.Remove(_currentTask);
            if (_gameTasks.IsEmpty())
            {
                _endScreen.gameObject.SetActive(true);
            }
            else
            {
                _currentTaskIndex = (_currentTaskIndex) % _gameTasks.Count;
                LaunchTask(_gameTasks[_currentTaskIndex]).Forget();
            }

        }
        else
        {
            _currentTaskIndex = (_currentTaskIndex + 1) % _gameTasks.Count;
            LaunchTask(_gameTasks[_currentTaskIndex]).Forget();
        }


    }

}

public class STaskLaunched
{
}

public class STaskEnded
{
    public bool isSuccess;
}

public class SUpdateHiddenStat
{
    public Ingredient.Modifier modifier;
    public StatType statType;
}