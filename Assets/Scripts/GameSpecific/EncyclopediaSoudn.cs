using FMODUnity;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EncyclopediaSoudn : MonoBehaviour
{
    [SerializeField] EventReference _close;
    [SerializeField] EventReference _open;
    [SerializeField] EventReference _page;
}
