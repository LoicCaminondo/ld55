using FMOD.Studio;
using FMODUnity;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using Zenject;

public class CauldronSound : MonoBehaviour
{
    [Inject] SignalBus _signalBus;

    [SerializeField] EventReference _beckon;
    [SerializeField] EventReference _idle;
    [SerializeField] EventReference _summon;
    [SerializeField] EventReference _ingSmall;
    [SerializeField] EventReference _ingMiddle;
    [SerializeField] EventReference _ingLarge;

    EventInstance _beckonInstance;
    EventInstance _idleInstance;

    // Start is called before the first frame update
    void Start()
    {
        _signalBus.Subscribe<SCldrnBoilSummoning>(SummonBoil);
        _signalBus.Subscribe<STaskEnded>(StartIdle);
        _signalBus.Subscribe<SIngredientEntered>(PlayIngredientEnteredSound);
        PickupItem.OnPickupItem.AddListener(StartBeckon);
        PickupItem.OnDropItem.AddListener(StopBeckon);
        _idleInstance = RuntimeManager.CreateInstance(_idle);
        _idleInstance.start();
    }

    private void PlayIngredientEnteredSound(SIngredientEntered sIngredientEntered)
    {
        Stat mass = sIngredientEntered.ingredient.Modif.Stats.First(x => x.Type == StatType.Mass);

        if (mass.Value < 2)
        {
            RuntimeManager.PlayOneShot(_ingSmall);
        }
        else if (mass.Value < 8)
        {
            RuntimeManager.PlayOneShot(_ingMiddle);
        }
        else
        {
            RuntimeManager.PlayOneShot(_ingLarge);
        }
    }

    private void StartIdle()
    {
        _idleInstance.start();
    }

    public void SummonBoil()
    {
        _idleInstance.stop(FMOD.Studio.STOP_MODE.ALLOWFADEOUT);
        RuntimeManager.PlayOneShot(_summon);
    }

    public void StartBeckon()
    {
        _beckonInstance = RuntimeManager.CreateInstance(_beckon);
        _beckonInstance.start();
    }

    public void StopBeckon()
    {
        _beckonInstance.stop(FMOD.Studio.STOP_MODE.IMMEDIATE);
        _beckonInstance.release();

    }
}
