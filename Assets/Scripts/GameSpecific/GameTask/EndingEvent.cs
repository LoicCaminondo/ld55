using Sirenix.OdinInspector;
using System.Collections;
using System.Collections.Generic;
using UnityEditor.Rendering;
using UnityEngine;

[CreateAssetMenu(fileName = "EndingEvent", menuName = "GameObj/EndingEvent"), InlineProperty]
public class EndingEvent : ScriptableObject
{
    [field: SerializeField, TextArea(4, 10)] public List<string> Descriptions { get; private set; }
    [field: SerializeField, ListDrawerSettings(ShowFoldout = false)] public List<StatChecker> StatCheckers { get; private set; }
    [field: SerializeField, ListDrawerSettings(ShowFoldout = false)] public bool IsSuccess { get; private set; } = false;





    public bool Test(List<Stat> stats)
    {
        foreach (var stateCheck in StatCheckers)
        {
            // If check don't work we fail test
            if (!stateCheck.Check(stats))
            {
                return (false);
            }


        }

        return (true);
    }
}

[System.Serializable]
public class StatChecker
{
    [field: SerializeField] public StatType statType;
    [field: SerializeField] public Comparison Comparison_ { get; private set; }
    [field: SerializeField] public int CheckValue { get; private set; }

    [field: SerializeField, ShowIf("IsClose")] public int ClosePrecision { get; private set; }

    public bool IsClose => Comparison_ == Comparison.Close;

    public bool Check(List<Stat> stats)
    {
        switch (Comparison_)
        {
            case Comparison.GreaterThan:
                return (GreaterComparison(stats));
            case Comparison.LowerThan:
                return (LowerComparison(stats));
            case Comparison.Close:
                return (CloseComparison(stats));
            default:
                return (false);
        };
    }

    private bool GreaterComparison(List<Stat> stats)
    {
        if (stats.Exists(x => x.Type == statType))
        {
            Stat stat = stats.Find(x => x.Type == statType);
            return (stat.Value >= CheckValue);
        }
        return (false);
    }

    private bool LowerComparison(List<Stat> stats)
    {
        if (stats.Exists(x => x.Type == statType))
        {
            Stat stat = stats.Find(x => x.Type == statType);
            return (stat.Value <= CheckValue);
        }
        return (true);
    }

    private bool CloseComparison(List<Stat> stats)
    {
        if (stats.Exists(x => x.Type == statType))
        {
            Stat stat = stats.Find(x => x.Type == statType);
            return (Mathf.Abs(stat.Value - CheckValue) <= ClosePrecision);
        }
        return (false);
    }
}

public enum Comparison
{
    GreaterThan,
    LowerThan,
    Close
}
