using Sirenix.OdinInspector;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "GameTask", menuName = "GameObj/GameTask")]
public class GameTask : ScriptableObject
{
    [field: SerializeField, TextArea(4, 10)] public List<string> Descriptions { get; private set; }

    [SerializeField, ListDrawerSettings(ShowFoldout = false)] EndingEvent _defaultEvent;

    [SerializeField, ListDrawerSettings(ShowFoldout = false)] List<PrioEvents> _endingEvents;

    public EndingEvent TryToSolve(List<Stat> monsterStats)
    {
        _endingEvents.Sort((x, y) => (x.Prio > y.Prio) ? 1 : -1);

        foreach (var entry in _endingEvents)
        {
            if (entry.endingEvents.Test(monsterStats))
            {
                // Here put return or something ? if we need to return info
                return (entry.endingEvents);
            }
        }
        if (_defaultEvent == null)
        {
            Debug.LogError("No default Events in task " + name);
            return (null);
        }

        return (_defaultEvent);
    }

}

[System.Serializable, InlineProperty]
public class PrioEvents
{
    public int Prio; // no need to prioritize as List are ordered
    public EndingEvent endingEvents;
}
