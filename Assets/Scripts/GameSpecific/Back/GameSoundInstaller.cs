using FMOD.Studio;
using FMODUnity;
using Sirenix.OdinInspector;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Zenject;

public class GameSoundInstaller : MonoInstaller
{
    public GameSoundManager.Settings settings;

    public override void InstallBindings()
    {
        Container.BindInterfacesAndSelfTo<GameSoundManager>().AsSingle();
        Container.BindInstance(settings).WhenInjectedInto<GameSoundManager>();
    }
}

public class GameSoundManager : IInitializable, IDisposable
{
    readonly List<DicoEvent> _dicoEvents;
    readonly SignalBus _signalBus;

    private Dictionary<string, EventInstance> _instanceDictionary;
    private EventInstance _musicInstance;

    public GameSoundManager(Settings settings, SignalBus signalBus)
    {
        _dicoEvents = settings.dicoEvents;
        _signalBus = signalBus;
    }

    public void Dispose()
    {
        _signalBus.Unsubscribe<ISoundSignal>(PlaySound);
        //_signalBus.Unsubscribe<IMusicSignal>(PlayMusic);

        //StopSound("Music");
    }

    public void Initialize()
    {
        _signalBus.Subscribe<ISoundSignal>(PlaySound);
        //_signalBus.Subscribe<IMusicSignal>(PlayMusic);
        _instanceDictionary = new Dictionary<string, EventInstance>();
        _musicInstance = new EventInstance();
        //PlayInstantiate("Music", _dicoEvents.Find(x => x.id == "Music").eventRef);
    }



    private void PlayMusic(IMusicSignal soundSignal)
    {
        Debug.Log(soundSignal.Id);
        if (!ValidateSound(soundSignal.Id))
        {
            Debug.LogError("Did not found sound with id " + soundSignal.Id);
            return;
        }

        if (_musicInstance.isValid())
        {
            _musicInstance.stop(FMOD.Studio.STOP_MODE.ALLOWFADEOUT);
            _musicInstance.release();
        }
        EventReference eventRef = (_dicoEvents.Find(x => x.id == soundSignal.Id)).eventRef;

        _musicInstance = RuntimeManager.CreateInstance(eventRef);
        _musicInstance.start();
    }

    private void PlaySound(ISoundSignal soundSignal)
    {
        if (!ValidateSound(soundSignal.Id))
        {
            Debug.LogError("Did not found sound with id " + soundSignal.Id);
            return;
        }

        EventReference eventRef = (_dicoEvents.Find(x => x.id == soundSignal.Id)).eventRef;
        if (soundSignal.ShouldPlay)
        {
            if (soundSignal.ShouldInstance)
            {
                PlayInstantiate(soundSignal.Id, eventRef);
            }
            else
            {
                PlayOneShot(eventRef);
            }
        }
        else
        {
            StopSound(soundSignal.Id);
        }
    }

    private bool ValidateSound(string id)
    {
        return (_dicoEvents.Exists(x => x.id == id));
    }

    private void PlayOneShot(EventReference eventRef)
    {
        RuntimeManager.PlayOneShot(eventRef);
    }

    private void PlayInstantiate(string id, EventReference eventRef)
    {
        EventInstance instance = RuntimeManager.CreateInstance(eventRef);
        instance.start();
        _instanceDictionary.Add(id, instance);

    }

    private void StopSound(string id)
    {
        var instance = _instanceDictionary[id];
        instance.stop(FMOD.Studio.STOP_MODE.ALLOWFADEOUT);
        instance.release();
        _instanceDictionary.Remove(id);
    }


    [Serializable]
    public class Settings
    {
        public List<DicoEvent> dicoEvents;
    }

    [Serializable]
    public struct DicoEvent
    {
        public string id;
        public FMODUnity.EventReference eventRef;
    }
}
