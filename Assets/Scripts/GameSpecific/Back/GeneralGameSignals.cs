using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SGameOver : ISoundSignal
{
    public string Id => "GameOver";
    public bool ShouldInstance => false;
    public bool ShouldPlay => true;
}

public class SStartGame : ISoundSignal
{
    public string Id => "Music";
    public bool ShouldInstance => true;
    public bool ShouldPlay => true;
}

namespace Signals
{
    public class SResumeGame
    {
        public bool shouldLaverLeCul;
    }

    public class SPauseGame
    {
    }

    public interface IPauseRequired
    {
    }
}