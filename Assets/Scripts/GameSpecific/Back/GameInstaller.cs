using UnityEngine;
using Zenject;

public class GameInstaller : MonoInstaller
{
    [Inject] private GameSettingsManager _gameSettingsManager;

    public override void InstallBindings()
    {
        _gameSettingsManager.InstallSettings(Container);

        Container.BindInterfacesAndSelfTo<GameManager>().AsSingle().NonLazy();
        Container.BindInterfacesAndSelfTo<LoadingGameManager>().AsSingle().NonLazy();
    }
}