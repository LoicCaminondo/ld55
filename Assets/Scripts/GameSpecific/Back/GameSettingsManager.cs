using System;
using System.Collections;
using System.Collections.Generic;
using Sirenix.OdinInspector;
using UnityEngine;
using Zenject;

public class GameSettingsManager
{
    public enum GameMode
    {
        Default
    }

    [Serializable]
    public class Settings
    {
        [SerializeField, InlineEditor] private GameSettingsSO _defaultGameSettings;

        public GameSettingsSO defaultGameSettings => _defaultGameSettings;
    }
    [Inject] private Settings _settings;

    private GameMode _gameMode = GameMode.Default;

    public void InstallSettings(DiContainer container)
    {
        GameSettingsSO gameSettings;

        gameSettings = _settings.defaultGameSettings;
    }

    public void SetGameMode(GameMode gameMode)
    {
        _gameMode = gameMode;
    }
}
