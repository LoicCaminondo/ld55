using System;
using UnityEngine;
using Zenject;

public class GameManager : IInitializable, IDisposable
{
    [Inject] private SignalBus _signalBus;

    public void Dispose()
    {
        Time.timeScale = 1;
    }

    public void Initialize()
    {
        Time.timeScale = 1;
        //_signalBus.Subscribe<GameOver>(OnGameOver);
    }

    private void OnGameOver()
    {
        Time.timeScale = 0;
    }
}
