using System.Collections;
using System.Collections.Generic;
using Cysharp.Threading.Tasks;
using Signals;
using UnityEngine;
using System.Linq;
using Zenject;
using System;

public class LoadingGameManager : IInitializable
{
    [Inject] private readonly SignalBus _signalBus;


    public async void Initialize()
    {
        await UniTask.Delay(TimeSpan.FromSeconds(1));
        _signalBus.Fire(new SLoadingDone());
    }


}
