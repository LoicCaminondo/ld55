using Cysharp.Threading.Tasks;
using FMODUnity;
using Sirenix.OdinInspector;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Zenject;

public class Client : MonoBehaviour
{
    [Inject] SignalBus _signalBus;

    [SerializeField] EventReference _loose;
    [SerializeField] EventReference _win;
    [SerializeField] EventReference _wait;
    [SerializeField] EventReference _order;
    [SerializeField] EventReference _make;

    [MinMaxSlider(10f, 100f, true), SerializeField] private Vector2 _timeSlider = new Vector2(30f, 50f);
    [MinMaxSlider(0f, 2f, true), SerializeField] private Vector2 _makeTimeSlider = new Vector2(0.5f, 1f);
    [Range(0f, 5f), SerializeField] private float _makeCooldown = 3f;

    private float _elapsedTime;
    private float _lastMakeTime;
    private float _currentTimeLimit;
    private System.Action _makeAction;

    private void Start()
    {
        _signalBus.Subscribe<SClientMake>(PlayMake);
        _signalBus.Subscribe<STaskEnded>(PlayEnded);
        _makeAction = () => DelayedPlayMake().Forget();
        _signalBus.Subscribe<SIngredientEntered>(_makeAction);
        _elapsedTime = 0f;
        _currentTimeLimit = Random.Range(_timeSlider.x, _timeSlider.y);
        PlayOrder();
    }

    private void OnDestroy()
    {
        _signalBus.TryUnsubscribe<SClientMake>(PlayMake);
        _signalBus.TryUnsubscribe<STaskEnded>(PlayEnded);
        _signalBus.TryUnsubscribe<SIngredientEntered>(_makeAction);
    }


    private void Update()
    {
        _elapsedTime += Time.deltaTime;
        if (_elapsedTime >= _currentTimeLimit)
        {
            _elapsedTime = 0f;
            _currentTimeLimit = Random.Range(_timeSlider.x, _timeSlider.y);
            PlayWait();
        }
    }


    private void PlayEnded(STaskEnded sTaskEnded)
    {
        if (sTaskEnded.isSuccess)
        {
            RuntimeManager.PlayOneShot(_win);
        }
        else
            RuntimeManager.PlayOneShot(_loose);
    }

    private async UniTaskVoid DelayedPlayMake()
    {
        await UniTask.WaitForSeconds(Random.Range(_makeTimeSlider.x, _makeTimeSlider.y));
        PlayMake();
    }

    private void PlayMake()
    {
        if (Random.Range(0f, _makeCooldown) < Time.time - _lastMakeTime)
        {
            RuntimeManager.PlayOneShot(_make);
            _lastMakeTime = Time.time;
        }
    }

    private void PlayOrder()
    {

        RuntimeManager.PlayOneShot(_order);
    }

    private void PlayWait()
    {
        RuntimeManager.PlayOneShot(_wait);
    }
}
