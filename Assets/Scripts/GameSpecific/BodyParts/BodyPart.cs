using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using Unity.VisualScripting;
using UnityEngine;
using Zenject;

public class BodyPart : MonoBehaviour
{


    [field: SerializeField] public BodyType BodyType { get; private set; }
    List<Slot> _slots;

    public int SlotNumber => _slots.Count;

    public void Init()
    {
        _slots = GetComponentsInChildren<Slot>().ToList();
    }


    public Slot FindSlot(BodyType bodyType)
    {
        if (_slots.Exists(x => x.IsFree && (bodyType & x.BodyType_) != 0))
        {
            return (_slots.Find(x => x.IsFree && (bodyType & x.BodyType_) != 0));
        }
        return (null);
    }


}


[Flags]
public enum BodyType
{
    None = 0,
    Head = 1,
    Arm = 2,
    Leg = 4,
    Body = 8,
    Horn = 16,
    Wings = 32,
    Eye = 64,
}