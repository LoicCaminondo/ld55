﻿using UnityEngine;

public class Slot : MonoBehaviour
{
    
    [field:SerializeField] public BodyType BodyType_ { get; private set; }

    public bool IsFree { get; private set; } = true;

    public void Lock()
    {
        IsFree = false;
    }
}