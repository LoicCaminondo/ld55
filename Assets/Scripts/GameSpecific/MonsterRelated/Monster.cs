using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Zenject;

public class Monster : MonoBehaviour
{
    [field: SerializeField] public List<Stat> MonsterStat { get; private set; } = new List<Stat>();
    public List<BodyPart> bodyParts { get; private set; } = new List<BodyPart>();
    public List<Ingredient.Modifier> Modifiers { get; private set; } = new List<Ingredient.Modifier>();

    public void SetStat(List<Stat> stats)
    {
        MonsterStat = stats;
    }

    public void SetModifiers(List<Ingredient.Modifier> modifiers)
    {
        Modifiers = modifiers;
    }

    public class Factory : PlaceholderFactory<Monster>
    {
    }
}
