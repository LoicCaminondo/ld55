using System;
using UnityEngine;

public class MonsterSpawner : MonoBehaviour
{
    public bool IsEnabled { get; private set; }

    public void Enable()
    {
        IsEnabled = true;
        GetComponent<Animator>().Play("Active");
    }

    public void Disable()
    {
        IsEnabled = false;
        GetComponent<Animator>().Play("Idle");
    }

    private void OnMouseDown()
    {
        if (IsEnabled)
        {
            SummoningMonsterAnimator.OnStartSummon?.Invoke();
            Disable();
        }
    }
}
