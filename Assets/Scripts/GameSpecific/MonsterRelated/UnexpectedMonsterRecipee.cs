using ModestTree;
using Sirenix.OdinInspector;
using Sirenix.Utilities;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;


[CreateAssetMenu(fileName = "UnexpectedRecipee", menuName = "GameObj/UnexpectedRecipee")]
public class UnexpectedMonsterRecipee : MonsterRecipee
{
    public override bool CheckRecipee(List<Ingredient.Modifier> modifiersInCauldron)
    {
        if (modifiersInCauldron.Count > 8)
            return (true);

        return (false);
    }
}
