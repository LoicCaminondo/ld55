using System;
using System.Collections;
using System.Collections.Generic;
using Unity.VisualScripting;
using UnityEngine;
using static UnityEngine.InputSystem.HID.HID;

using Zenject;

public class MonsterFactory : IFactory<Monster>
{
    [Serializable]
    public class Settings
    {
    }

    [Inject] readonly DiContainer _container;

    public Monster  Create()
    {
        return _container.InstantiateComponentOnNewGameObject<Monster>("Monster");
    }
}
