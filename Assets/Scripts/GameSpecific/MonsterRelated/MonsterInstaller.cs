using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Zenject;

public class MonsterInstaller : MonoInstaller
{
    public override void InstallBindings()
    {
        Container.BindFactory<Monster, Monster.Factory>().FromFactory<MonsterFactory>();
        Container.Bind<MonsterManager>().AsSingle();
    }
}

public class MonsterManager
{
    public MonsterManager()
    {

    }
}

public class SMonsterBrewed : ISoundSignal
{
    public string Id => "SummoningComplete";
    public bool ShouldInstance => false;
    public bool ShouldPlay => true;
    public Monster brewedMonster;
}