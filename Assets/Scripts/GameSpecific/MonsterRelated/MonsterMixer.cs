using Cysharp.Threading.Tasks.Triggers;
using ModestTree;
using MoreMountains.Feedbacks;
using Sirenix.OdinInspector;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEditor;
using UnityEngine;
using Zenject;

public class MonsterMixer : MonoBehaviour
{
    [Inject] Monster.Factory _monsterFactory;
    [Inject] SignalBus _signalBus;
    [Inject] DayNightManager _dayNightManager;
    [Inject] RitualMusicManager _ritualMusicManager;
    [Inject(Id = "MonsterAnchor")] Transform _monsterAnchor;
    [Inject] MonsterSpawner _monsterSpawner;

    [SerializeField] BodyPart _defaultBody;
    [SerializeField] GameObject _blob;
    [SerializeField] Collider2D _itemDetectorCollider;
    [SerializeField] GameObject _itemBlocker;

    [ShowInInspector, ReadOnly] public List<Ingredient.Modifier> _modifiers { get; private set; } = new List<Ingredient.Modifier>();

    [SerializeField] List<MonsterRecipee> _monsterRecipees;

    private void Start()
    {
        UnblockCauldron();
    }

    private void BlockCauldron()
    {
        _itemDetectorCollider.enabled = false;
        _itemBlocker.SetActive(true);
    }

    private void UnblockCauldron()
    {
        _itemDetectorCollider.enabled = true;
        _itemBlocker.SetActive(false);
    }

    [Button("Add Ingredient", Expanded = true)]
    public void AddIngredient(Ingredient ingredient)
    {
        _signalBus.Fire(new SIngredientEntered() { ingredient = ingredient });
        // TODO: Here there will be a signal (at least for the sound)
        // use this occasion to detach Spawner from the mixer and use the signals to communicate
        if (!_monsterSpawner.IsEnabled)
            _monsterSpawner.Enable();
        _modifiers.Add(ingredient.Modif);
        if (_modifiers.Where(x => x.BodyPart != null).Count() > 8)
        {
            BlockCauldron();
        }
    }

    private void OnTriggerEnter2D(Collider2D other)
    {
        if (other.TryGetComponent(out Ingredient ingredient))
        {
            AddIngredient(ingredient);
            if (other.TryGetComponent(out Respawnable respawnable))
            {
                respawnable.Respawn();
            }
            else
            {
                Destroy(other.gameObject);
            }
        }
    }

    public void AddBodyPart(Monster monster, BodyPart bodyPart)
    {
        var newBodyPart = GameObject.Instantiate(bodyPart, monster.transform);
        newBodyPart.Init();
        if (monster.bodyParts.IsEmpty())
        {
            monster.bodyParts.Add(newBodyPart);
        }
        else
        {
            Slot slot = FindFreeSlot(monster, newBodyPart);
            if (slot != null)
            {
                slot.Lock();
                newBodyPart.transform.position = slot.transform.position;
                newBodyPart.transform.rotation = slot.transform.rotation;
                monster.bodyParts.Add(newBodyPart);
            }
            else
            {
                GameObject.Destroy(newBodyPart.gameObject);
                Debug.LogError("Slot null");
            }
        }
    }

    private Slot FindFreeSlot(Monster monster, BodyPart bodyPart)
    {
        Slot slot = null;
        int i = 0;

        while (i < monster.bodyParts.Count && slot == null)
        {
            slot = monster.bodyParts[i].FindSlot(bodyPart.BodyType);
            i++;
        }
        return (slot);
    }

    [Button("Brew")]
    public void Brew()
    {
        _monsterSpawner.Disable();
        _modifiers.Sort();
        Monster monster = null;

        foreach (var recipee in _monsterRecipees)
        {
            if (recipee.CheckRecipee(_modifiers))
            {
                _signalBus.Fire(new SRecipeBrewed(recipee));
                monster = recipee.SpawnPremadeMonster();
                break;
            }
        }
        if (monster == null)
        {
            monster = _monsterFactory.Create();
            if (_modifiers.Find(x => x.BodyPart != null && x.BodyPart.BodyType.HasFlag(BodyType.Body)) == null)
            {
                AddBodyPart(monster, _defaultBody);
            }
            foreach (var modification in _modifiers)
            {
                if (modification.BodyPart != null)
                {
                    AddBodyPart(monster, modification.BodyPart);
                }
            }
            List<Stat> finalMonsterStats = GetCurrentMonsterStats();
            //foreach (var modification in _modifiers)
            //{
            //    foreach (var stat in modification.Stats)
            //    {
            //        if (finalMonsterStats.Exists(x => x.Type == stat.Type))
            //        {
            //            finalMonsterStats.Find(x => x.Type == stat.Type).Add(stat.Value);
            //        }
            //        else
            //        {
            //            finalMonsterStats.Add(new Stat(stat.Type, stat.Value));
            //        }
            //    }
            //}

            _dayNightManager.ModifyStats(finalMonsterStats);
            _ritualMusicManager.ModifyStats(finalMonsterStats);
            monster.SetStat(finalMonsterStats);

        }

        monster.SetModifiers(new List<Ingredient.Modifier>(_modifiers));

        _modifiers.Clear();

        monster.transform.SetParent(_monsterAnchor);
        monster.transform.localPosition = Vector3.zero;
        _signalBus.AbstractFire(new SMonsterBrewed() { brewedMonster = monster });
        UnblockCauldron();
    }

    public List<Stat> GetCurrentMonsterStats()
    {
        List<Stat> finalMonsterStats = new List<Stat>();
        foreach (var modification in _modifiers)
        {
            foreach (var stat in modification.Stats)
            {
                if (finalMonsterStats.Exists(x => x.Type == stat.Type))
                {
                    finalMonsterStats.Find(x => x.Type == stat.Type).Add(stat.Value);
                }
                else
                {
                    finalMonsterStats.Add(new Stat(stat.Type, stat.Value));
                }
            }
        }
        return (finalMonsterStats);
    }

}

public class SRecipeBrewed
{
    public MonsterRecipee MonsterRecipee { get; private set; }

    public SRecipeBrewed(MonsterRecipee monsterRecipee)
    {
        MonsterRecipee = monsterRecipee;
    }
}


public class SIngredientEntered
{
    public Ingredient ingredient;
}