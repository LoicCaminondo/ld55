using ModestTree;
using Sirenix.OdinInspector;
using Sirenix.Utilities;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;


[CreateAssetMenu(fileName = "Recipee", menuName = "GameObj/Recipee")]
public class MonsterRecipee : ScriptableObject
{
    [field: SerializeField, ListDrawerSettings(ShowFoldout = false)] public List<Ingredient> Ingredients { get; private set; } = new List<Ingredient>();
    [field: SerializeField] public DayNight dayNight { get; private set; }

    [SerializeField] GameObject _premadeMonsterPrefab;

    public virtual bool CheckRecipee(List<Ingredient.Modifier> modifiersInCauldron)
    {
        if (Ingredients.Count != modifiersInCauldron.Count)
            return (false);

        var ingredientRecipe = new List<Ingredient.Modifier>(Ingredients.Select(ingredient => ingredient.Modif));
        var cauldronChecker = new List<Ingredient.Modifier>(modifiersInCauldron);



        while (!ingredientRecipe.IsEmpty())
        {
            var currentIngredientRecipe = ingredientRecipe[0];
            int indexInCauldron = cauldronChecker.FindIndex(x => x.Id == currentIngredientRecipe.Id);
            if (indexInCauldron >= 0)
            {
                cauldronChecker.RemoveAt(indexInCauldron);
                ingredientRecipe.RemoveAt(0);
            }
            else
            {
                return (false);
            }
        }

        if (dayNight != DayNight.None)
        {
            // Check if Day night do the good things
        }

        return (true);
    }

    public Monster SpawnPremadeMonster()
    {
        Monster monster = GameObject.Instantiate(_premadeMonsterPrefab).GetComponent<Monster>();
        return (monster);
    }
}

public enum DayNight
{
    None,
    Day,
    Night
}
