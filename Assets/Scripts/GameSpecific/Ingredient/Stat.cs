using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class Stat
{

    [field: SerializeField] public StatType Type { get; private set; }

    [field: SerializeField] public int Value { get; private set; }

    [field: SerializeField] public bool IsHidden { get; private set; } = true;

    [field: SerializeField] public bool IsTrueHidden { get; private set; } = false;
    public Stat()
    {
        IsHidden = true;
    }

    public Stat(StatType type, int value)
    {
        Type = type;
        Value = value;
        IsHidden = true;
    }

    public void Find()
    {
        IsHidden = false;
    }

    public void Hide()
    {
        IsHidden = true;
    }

    public void Add(int toAdd)
    {
        Value += toAdd;
    }

    public void Mult(int toMult)
    {
        Value *= toMult;
    }

    public void Div(int toDiv)
    {
        Value /= toDiv;
    }

    static public List<Stat> GetDefaultStats()
    {
        List<Stat> defaultStats = new List<Stat>
        {
            new Stat(StatType.Strength, 0),
            new Stat(StatType.Motricity, 0),
            new Stat(StatType.CarnivorousInstinct, 0),
            new Stat(StatType.Speed, 0),
            new Stat(StatType.Intel, 0),
            new Stat(StatType.Discretion, 0)
        };
        return defaultStats;
    }

    public string ToNiceString()
    {
        string niceValue;
        string niceName;

        if (Value < 0)
            niceValue = "<color=#FF0004>" + new string('-', Mathf.Min(Value, 3)) + "</color>";
        else if (Value > 0)
            niceValue = "<color=#007704>" + new string('+', Mathf.Min(Value, 3)) + "</color>";
        else
            niceValue = "<color=#FF0004>" + "X" + "</color>";

        switch (Type)
        {
            case StatType.Strength:
                niceName = "Strength";
                break;
            case StatType.Motricity:
                niceName = "Motricity";
                break;
            case StatType.Limb:
                niceName = "Limb";
                break;
            case StatType.CarnivorousInstinct:
                niceName = "Animal instinct";
                break;
            case StatType.Speed:
                niceName = "Speed";
                break;
            case StatType.Intel:
                niceName = "Intelligence";
                break;
            case StatType.Banana:
                niceName = "Banana";
                break;
            case StatType.Discretion:
                niceName = "Discretion";
                break;
            default:
                niceName = "Not Implemented";
                break;
        }
        return (niceName + " " + niceValue);
    }
}

public enum StatType
{
    Strength,
    Motricity,
    Limb,
    CarnivorousInstinct,
    Speed,
    Discretion,
    Intel,
    Banana,
    Druid,
    Arcana,
    Mass

}


