using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using MoreMountains.Feedbacks;
using Sirenix.OdinInspector;
using Unity.VisualScripting;
using Unity.VisualScripting.Antlr3.Runtime.Misc;
using UnityEditor;
using UnityEngine;
using Zenject;

public class Ingredient : MonoBehaviour
{
    [Inject] SignalBus _signalBus;
    [SerializeField] private int _numberOfSuccessToUnlock = 0;

    [Inject(Id = "MonsterStat")] private InfoBubble _statBubble;
    private Notif _notif;

    [Serializable]
    public class Modifier : IComparable<Modifier>
    {
        [field: SerializeField, ReadOnly] public string Id { get; private set; } = Guid.NewGuid().ToString();

        [OnCollectionChanged("NewID")]
        [SerializeField] private List<Stat> _stat = new();
        public ReadOnlyCollection<Stat> Stats => new ReadOnlyCollection<Stat>(_stat);
        [field: SerializeField] public BodyPart BodyPart { get; private set; }


        private void NewID() => Id = Guid.NewGuid().ToString();

        public void CompleteStatsWithDefaults()
        {
            List<Stat> defaultStats = Stat.GetDefaultStats();
            foreach (Stat stat in defaultStats)
            {
                if (!_stat.Any(s => s.Type == stat.Type))
                    _stat.Add(stat);
            }
        }

        public int CompareTo(Modifier other)
        {
            if (BodyPart && (other.BodyPart == null || BodyPart.SlotNumber > other.BodyPart.SlotNumber
                || (BodyPart.SlotNumber == other.BodyPart.SlotNumber && BodyPart.BodyType < other.BodyPart.BodyType)))
                return (-1);
            return GetHashCode().CompareTo(other.GetHashCode());
        }
    }

    [field: SerializeField] public Modifier Modif { get; private set; } = new Modifier();

    private void Awake()
    {
        _signalBus.Subscribe<STaskLaunched>(OnTaskLaunched);
        _signalBus.Subscribe<STaskEnded>(OnTaskEnded);
        _signalBus.Subscribe<SUpdateHiddenStat>(UpdateHiddenStat);
        _notif = GetComponent<Notif>();
        Modif.CompleteStatsWithDefaults();
        //foreach (var stat in Modif.Stats)
        //{
        //    stat.Hide();
        //}
        if (_numberOfSuccessToUnlock > 0)
            gameObject.SetActive(false);
    }

    private void UpdateHiddenStat(SUpdateHiddenStat sUpdateHiddenStat)
    {
        if (sUpdateHiddenStat.modifier.Id == Modif.Id)
        {
            foreach (var stat in Modif.Stats)
            {
                if (stat.Type == sUpdateHiddenStat.statType && stat.IsHidden)
                {
                    stat.Find();
                    _notif.ActiveNotif();
                    break;
                }
            }
        }
    }

    public void UpdateStat()
    {
        bool stuffIsHidden = false;
        string statsText = $"{name} properties:\n";

        foreach (Stat stat in Modif.Stats)
        {
            if (stat.IsHidden && !stat.IsTrueHidden)
                stuffIsHidden = true;
            else if (!stat.IsTrueHidden)
                statsText += "- " + stat.ToNiceString() + "\n";
        }


        if (stuffIsHidden)
            statsText += "<wave><color=#FF0004> To discover </color></wave>";
        _statBubble.TextObj.text = statsText;


    }

    public void OnTaskLaunched()
    {
        _statBubble.Enable();
    }

    public void OnTaskEnded(STaskEnded sTaskEnded)
    {
        if (sTaskEnded.isSuccess && _numberOfSuccessToUnlock > 0)
        {
            _numberOfSuccessToUnlock -= 1;
            if (_numberOfSuccessToUnlock == 0)
                gameObject.SetActive(true);
        }
    }

    private bool _isOn = false;

    private void OnMouseOver()
    {
        if (!_isOn)
        {
            _isOn = true;
            UpdateStat();
            _notif.DeactiveNotif();
            _statBubble.OnMouseOver();
        }
    }

    private void OnMouseExit()
    {
        if (_isOn)
        {
            _isOn = false;
            //_statBubble.OnMouseExit();
        }

    }

    public void OnRespawn()
    {
        _statBubble.Enable();
    }
}
