using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Zenject;

public class RespawnWhenMonsterBrewed : MonoBehaviour
{
    [Inject] readonly SignalBus _signalBus;

    void Start()
    {
        _signalBus.Subscribe<SStartingBrewing>(OnStartingBrewing);
    }

    private void OnStartingBrewing()
    {
        if (GetComponent<Rigidbody2D>().bodyType != RigidbodyType2D.Static)
        {
            GetComponent<Respawnable>().Respawn();
        }
    }
}
