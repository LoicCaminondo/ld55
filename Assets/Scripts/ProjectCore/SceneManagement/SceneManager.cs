using System;
using System.Threading;
using Cysharp.Threading.Tasks;
using Eflatun.SceneReference;
using Signals;
using TransitionsPlus;
using UnityEngine;
using UnityEngine.SceneManagement;
using Zenject;

namespace ProjectCore
{
    public class SceneManager : ISceneManager
    {
        [Serializable]
        public class Settings
        {
            [SerializeField] private TransitionProfile _sceneEnter;
            [SerializeField] private TransitionProfile _sceneExit;
            [SerializeField] private SceneReference _gameScene = null;

            public TransitionProfile SceneEnter => _sceneEnter;
            public TransitionProfile SceneExit => _sceneExit;
            public SceneReference GameScene => _gameScene;
        }
        [Inject] private readonly Settings _settings;
        [Inject] private readonly SignalWaiter<SLoadingDone>.Factory _loadingDoneSignalWaiterFactory;

        private CancellationTokenSource _cancellationTokenSource;

        public async UniTask LoadScene(SceneReference scene, CancellationToken cancellationToken = default)
        {
            await UnityEngine.SceneManagement.SceneManager.LoadSceneAsync(scene.Path, LoadSceneMode.Single).WithCancellation(cancellationToken);
        }

        public async UniTask AdditiveLoadScene(SceneReference scene, CancellationToken cancellationToken = default)
        {
            await UnityEngine.SceneManagement.SceneManager.LoadSceneAsync(scene.Path, LoadSceneMode.Additive).WithCancellation(cancellationToken);
        }

        public async UniTask LoadGame()
        {
            await FadeOut();
            await LoadScene(_settings.GameScene);
            await FadeIn();
        }

        private async UniTask Transition(TransitionProfile profile, CancellationToken cancellationToken = default)
        {
            TransitionAnimator transitionAnimator = TransitionAnimator.Start(profile, useUnscaledTime: true);
            await UniTask.WaitUntil(() => transitionAnimator.progress >= 1, cancellationToken: cancellationToken);
        }

        private async UniTask FadeIn(CancellationToken cancellationToken = default)
        {
            await Transition(_settings.SceneEnter, cancellationToken);
        }

        private async UniTask FadeOut(CancellationToken cancellationToken = default)
        {
            await Transition(_settings.SceneExit, cancellationToken);
        }
    }
}