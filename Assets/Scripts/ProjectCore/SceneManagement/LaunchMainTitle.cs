using FMOD;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Zenject;

public class LaunchMainTitle : MonoBehaviour
{
    [Inject] MusicManager _musicMain;

    private void OnEnable()
    {
        _musicMain.Enable().Forget();
    }

    private void OnDisable()
    {
        _musicMain.Disable();
    }
}
