using System.Threading;
using Cysharp.Threading.Tasks;
using Eflatun.SceneReference;

public interface ISceneManager
{
    public UniTask LoadScene(SceneReference scene, CancellationToken cancellationToken = default);
    public UniTask AdditiveLoadScene(SceneReference scene, CancellationToken cancellationToken = default);

    public UniTask LoadGame();
}