using System;
using System.Collections;
using System.Collections.Generic;
using Cysharp.Threading.Tasks;
using Signals;
using UnityEngine;
using Zenject;

public class GameLoadingScreen : MonoBehaviour
{
    [Inject] private readonly SignalBus _signalBus;

    [SerializeField] private float _disapearitionDelay = 1f;

    private CanvasGroup _canvasGroup;

    void Start()
    {
        _canvasGroup = GetComponent<CanvasGroup>();
        _signalBus.Subscribe<SLoadingDone>(() => OnLoadingDone().Forget());
    }

    private async UniTaskVoid OnLoadingDone()
    {
        while (_canvasGroup.alpha > 0)
        {
            _canvasGroup.alpha -= Time.unscaledDeltaTime / _disapearitionDelay;
            await UniTask.Yield();
        }
        Destroy(gameObject);
    }
}
