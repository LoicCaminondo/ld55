using System.Collections;
using System.Collections.Generic;
using System.Threading;
using Cysharp.Threading.Tasks;
using FMODUnity;
using Unity.Collections;
using UnityEngine;
using UnityEngine.InputSystem;
using UnityEngine.InputSystem.Utilities;
using Zenject;

public class TitleScreen : MonoBehaviour
{
    [SerializeField] private TMPro.TextMeshProUGUI _continueText;
    [SerializeField] private int _waitingTimeMs = 3000;
    [SerializeField] private EventReference _uiButtonPressSound;

    [Inject] private readonly ISceneManager _sceneManager;
    async void Start()
    {
        CancellationTokenSource cts = new CancellationTokenSource();
        System.IDisposable callOnce = InputSystem.onAnyButtonPress.CallOnce((inputControl) => cts.Cancel());
        await UniTask.Delay(_waitingTimeMs, cancellationToken: cts.Token).SuppressCancellationThrow();
        callOnce.Dispose();
        _continueText.gameObject.SetActive(true);
        await UniTask.Delay(100);
        InputSystem.onAnyButtonPress.CallOnce((inputControl) =>
        {
            RuntimeManager.PlayOneShot(_uiButtonPressSound);
            _sceneManager.LoadGame();
        });
    }
}
