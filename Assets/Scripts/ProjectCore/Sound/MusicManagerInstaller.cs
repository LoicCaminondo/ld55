using Cysharp.Threading.Tasks;
using FMOD.Studio;
using FMODUnity;
using System;
using System.Collections;
using UnityEngine;
using Zenject;

public class MusicMainTitleInstaller : MonoInstaller
{
    [SerializeField] private MusicManager.Parameters _parameters;


    public override void InstallBindings()
    {
        Container.BindInterfacesAndSelfTo<MusicManager>().AsSingle().NonLazy();
        Container.BindInstance(_parameters).WhenInjectedInto<MusicManager>();
    }
}

public class MusicManager : IInitializable
{
    [Serializable]
    public class Parameters
    {
        [field: SerializeField] public EventReference MainTitleMusic { get; private set; }
        [field: SerializeField] public EventReference GameMusic { get; private set; }
    }

    EventInstance _instance;
    [Inject] SignalBus _signalBus;
    [Inject] Parameters _parameters;
    [Inject] AsyncProcessor _asyncProcessor;

    private BackgroundMusicType _backgroundMusic;
    private string _currentParameterName;

    private bool _initialized = false;

    public async UniTaskVoid Enable()
    {
        RuntimeManager.LoadBank("Master");
        RuntimeManager.LoadBank("Music");
        await UniTask.WaitUntil(() => RuntimeManager.HasBankLoaded("Master") && RuntimeManager.HasBankLoaded("Music"));
        if (!_instance.isValid())
        {
            _instance = RuntimeManager.CreateInstance(_parameters.MainTitleMusic);
            _instance.start();
            _signalBus.Subscribe<BackgroundStartMusicSignal>(StartMusic);
            _signalBus.Subscribe<SBackgroundChangeMusicSignal>(ChangeMusic);
            _signalBus.Subscribe<SBackgroundStopMusicSignal>(StopMusic);
            _signalBus.Subscribe<SStartingBrewing>(SumonningChange);
            _signalBus.Subscribe<SFadeOut>(StartSubtilFadeOut);
            _initialized = true;
        }
    }

    public async UniTaskVoid SwitchToGameMusic()
    {
        await UniTask.WaitUntil(() => _initialized);
        _instance.stop(FMOD.Studio.STOP_MODE.ALLOWFADEOUT);
        _instance.release();
        _instance = RuntimeManager.CreateInstance(_parameters.GameMusic);
        GoBackToDefault();
    }

    private void StartSubtilFadeOut()
    {
        _asyncProcessor.StartCoroutine(SubtilFadeOut());
    }

    public void GoBackToDefault()
    {
        _backgroundMusic = BackgroundMusicType.Default;
        _currentParameterName = "";
        _instance.start();
        _instance.setVolume(1f);

        _instance.setParameterByName("Druid Chant", 0);
        _instance.setParameterByName("Arcane Chant", 0);
        _instance.setParameterByName("Sorcerer Chant", 0);
    }

    private void StartMusic(BackgroundStartMusicSignal backgroundMusicSignal)
    {
        _instance.start();
        _currentParameterName = "";
    }

    public void SwitchDayNight(DayNight dayNight)
    {
        if (dayNight == DayNight.Day)
        {
            _instance.setParameterByName("Night", 0);
        }
        else if (dayNight == DayNight.Night)
        {
            _instance.setParameterByName("Night", 1);
        }
    }


    private void StopMusic(SBackgroundStopMusicSignal backgroundMusicSignal)
    {
        _instance.stop(FMOD.Studio.STOP_MODE.ALLOWFADEOUT);
        _instance.release();
    }
    private void ChangeMusic(SBackgroundChangeMusicSignal backgroundMusicSignal)
    {

        switch (backgroundMusicSignal.Id)
        {
            case BackgroundMusicType.Default:
                {
                    _backgroundMusic = BackgroundMusicType.Default;
                    _currentParameterName = "";
                    _instance.setParameterByName("Druid Chant", 0);
                    _instance.setParameterByName("Arcane Chant", 0);
                    _instance.setParameterByName("Sorcerer Chant", 0);
                    break;
                }
            case BackgroundMusicType.Druid:
                {
                    _backgroundMusic = BackgroundMusicType.Druid;
                    _currentParameterName = "Druid Chant";
                    _instance.setParameterByName("Druid Chant", 0.97f);
                    _instance.setParameterByName("Arcane Chant", 0);
                    _instance.setParameterByName("Sorcerer Chant", 0);
                    break;
                }
            case BackgroundMusicType.Arcane:
                {
                    _backgroundMusic = BackgroundMusicType.Arcane;
                    _currentParameterName = "Arcane Chant";
                    _instance.setParameterByName("Druid Chant", 0);
                    _instance.setParameterByName("Arcane Chant", 0.98f);
                    _instance.setParameterByName("Sorcerer Chant", 0);
                    break;
                }
            case BackgroundMusicType.Sorcerer:
                {
                    _backgroundMusic = BackgroundMusicType.Sorcerer;
                    _currentParameterName = "Sorcerer Chant";
                    _instance.setParameterByName("Druid Chant", 0);
                    _instance.setParameterByName("Arcane Chant", 0);
                    _instance.setParameterByName("Sorcerer Chant", 0.95f);
                    break;
                }
        }


        //_instance.getParameterByName(backgroundMusicSignal.Id, out float value);
        //_instance.setParameterByName(backgroundMusicSignal.Id, value);
    }

    public void SumonningChange()
    {
        _instance.setParameterByName(_currentParameterName, 1f);
    }


    /*
    private UniTask FadeIn(float fade)
    {
        float timer = duration;
        float increment = 0.8f / duration;

        isFading = true;
        while (timer > 0.0f)
        {
            float delta = Time.deltaTime;
            fade += delta * increment;
            timer -= delta;

            yield return null;
        }
        isFading = false;
    }
    */

    public void Disable()
    {
        _signalBus.Unsubscribe<SBackgroundChangeMusicSignal>(ChangeMusic);
        //if (_instance.isValid())
        //{
        //    _instance.stop(FMOD.Studio.STOP_MODE.ALLOWFADEOUT);
        //    _instance.release();
        //}

    }

    public void Initialize()
    {
        Enable().Forget();
    }



    public IEnumerator SubtilFadeOut()
    {
        float t = 1f;
        while (t >= 0f)
        {
            _instance.setVolume(t);
            t -= Time.deltaTime;
            yield return null;
        }
        yield return null;
    }

}

public enum BackgroundMusicType
{
    Default,
    Druid,
    Arcane,
    Sorcerer
}

public class SFadeOut { }