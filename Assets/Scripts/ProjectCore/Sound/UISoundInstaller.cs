using FMODUnity;
using System;
using UnityEngine;
using Zenject;

public class UISoundInstaller : MonoInstaller
{
    public UISoundManager.Settings settings;

    public override void InstallBindings()
    {
        Container.BindInterfacesAndSelfTo<UISoundManager>().AsSingle().NonLazy();
        Container.BindInstance(settings).WhenInjectedInto<UISoundManager>();
    }
}

public class UISoundManager : IInitializable, IDisposable
{
    readonly SignalBus _signalBus;

    [Serializable]
    public class Settings
    {
        [field: SerializeField] public EventReference HoverEvent { get; private set; }
        [field: SerializeField] public EventReference ClickEvent { get; private set; }
        [field: SerializeField] public EventReference PanelEvent { get; private set; }
        [field: SerializeField] public EventReference SliderEvent { get; private set; }
    }
    readonly private Settings _settings;

    public UISoundManager(SignalBus signalBus, Settings settings)
    {
        _signalBus = signalBus;
        _settings = settings;
    }


    public void Initialize()
    {
        _signalBus.Subscribe<FSendUISignal.STriggerHoverButtonSound>(PlayHover);
        _signalBus.Subscribe<FSendUISignal.STriggerClickButtonSound>(PlayClick);
        _signalBus.Subscribe<FSendUISignal.STriggerPanelSound>(PlayPanel);
        _signalBus.Subscribe<FSendUISignal.STriggerSliderSound>(PlaySlider);
    }


    public void Dispose()
    {
        _signalBus.Unsubscribe<FSendUISignal.STriggerHoverButtonSound>(PlayHover);
        _signalBus.Unsubscribe<FSendUISignal.STriggerClickButtonSound>(PlayClick);
        _signalBus.Unsubscribe<FSendUISignal.STriggerPanelSound>(PlayPanel);
        _signalBus.Unsubscribe<FSendUISignal.STriggerSliderSound>(PlaySlider);
    }


    private void PlayHover()
    {

        FMODUnity.RuntimeManager.PlayOneShot(_settings.HoverEvent);
    }

    private void PlayClick()
    {
        FMODUnity.RuntimeManager.PlayOneShot(_settings.ClickEvent);
    }

    private void PlayPanel()
    {
        FMODUnity.RuntimeManager.PlayOneShot(_settings.PanelEvent);
    }

    private void PlaySlider()
    {
        RuntimeManager.PlayOneShot(_settings.SliderEvent);
    }


}