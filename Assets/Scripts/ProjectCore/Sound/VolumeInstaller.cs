using FMOD.Studio;
using FMODUnity;
using MoreMountains.Tools;
using Sirenix.OdinInspector;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Zenject;

public class VolumeInstaller : Installer<VolumeInstaller>
{
    public override void InstallBindings()
    {
        Container.BindInterfacesAndSelfTo<VolumeManager>().AsSingle().NonLazy();
    }
}

public class VolumeManager : IInitializable, IDisposable
{
    private Bus _soundEffectBus;
    private Bus _musicBus;

    public float SoundVolume
    {
        get
        {
            var result = _soundEffectBus.getVolume(out float soundVolume);
            return (soundVolume);
        }
        set
        {
            _soundEffectBus.setVolume(value);
        }
    }

    public float MusicVolume
    {
        get
        {
            var result = _musicBus.getVolume(out float soundVolume);
            return (soundVolume);
        }
        set
        {
            _musicBus.setVolume(value);
        }
    }

    public void Dispose()
    {
        PlayerPrefs.SetFloat("MusicVolume", MusicVolume);
        PlayerPrefs.SetFloat("SoundVolume", SoundVolume);
    }

    public void Initialize()
    {
        _soundEffectBus = RuntimeManager.GetBus("Bus:/SoundEffect");
        _musicBus = RuntimeManager.GetBus("Bus:/Music");

        //if (PlayerPrefs.HasKey("MusicVolume"))
        //    MusicVolume = PlayerPrefs.GetFloat("MusicVolume");
        //else
        //    MusicVolume = 1f;
        //if (PlayerPrefs.HasKey("SoundVolume"))
        //    SoundVolume = PlayerPrefs.GetFloat("SoundVolume");
        //else
        //    SoundVolume = 1f;
    }

}
