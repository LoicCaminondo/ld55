using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public interface ISoundSignal
{
    public string Id { get; }
    public bool ShouldInstance { get; }
    public bool ShouldPlay { get; }
}

public class BackgroundStartMusicSignal { }
public class SBackgroundChangeMusicSignal
{
    public BackgroundMusicType Id { get; }
    public SBackgroundChangeMusicSignal(BackgroundMusicType id)
    {
        Id = id;
    }
}

public class SBackgroundStopMusicSignal
{
}

public interface IMusicSignal
{
    public string Id { get; }
}

public class SClientMake
{

}

public class SClientEnter
{

}

public class SAlive : ISoundSignal
{
    public string Id => "Alive";
    public bool ShouldInstance => false;
    public bool ShouldPlay => true;
}

public class SCldrnBoilIdle : ISoundSignal
{
    public string Id => "CldrnBoilIdle";
    public bool ShouldInstance => false;
    public bool ShouldPlay => true;
}


public class SCldrnBoilBeckon : ISoundSignal
{
    public string Id => "CldrnBoilBeckon";
    public bool ShouldInstance => false;
    public bool ShouldPlay => true;
}


public class SCldrnBoilActive : ISoundSignal
{
    public string Id => "CldrnBoilActive";
    public bool ShouldInstance => false;
    public bool ShouldPlay => true;
}


public class SCldrnBoilSummoning
{
}


public class SIngredDropCldrnBig : ISoundSignal
{
    public string Id => "IngredDropCldrnBig";
    public bool ShouldInstance => false;
    public bool ShouldPlay => true;
}

public class SIngredDropCldrnMedium : ISoundSignal
{
    public string Id => "IngredDropCldrnMedium";
    public bool ShouldInstance => false;
    public bool ShouldPlay => true;
}

public class SIngredDropCldrnSmall : ISoundSignal
{
    public string Id => "IngredDropCldrnSmall";
    public bool ShouldInstance => false;
    public bool ShouldPlay => true;
}

public class SIngredBin : ISoundSignal
{
    public string Id => "IngredBin";
    public bool ShouldInstance => false;
    public bool ShouldPlay => true;
}

public class SIngredPickup : ISoundSignal
{
    public string Id => "IngredPickup";
    public bool ShouldInstance => false;
    public bool ShouldPlay => true;
}


public class SCustomMoveIn : ISoundSignal
{
    public string Id => "CustomMoveIn";
    public bool ShouldInstance => false;
    public bool ShouldPlay => true;
}


public class SCustomMoveOut : ISoundSignal
{
    public string Id => "CustomMoveOut";
    public bool ShouldInstance => false;
    public bool ShouldPlay => true;
}


public class SBookOpen : ISoundSignal
{
    public string Id => "BookOpen";
    public bool ShouldInstance => false;
    public bool ShouldPlay => true;
}

public class SBookPage : ISoundSignal
{
    public string Id => "BookPage";
    public bool ShouldInstance => false;
    public bool ShouldPlay => true;
}

public class SBookClose : ISoundSignal
{
    public string Id => "BookClose";
    public bool ShouldInstance => false;
    public bool ShouldPlay => true;
}