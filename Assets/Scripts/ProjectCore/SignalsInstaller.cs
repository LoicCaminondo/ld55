using Signals;
using UnityEngine;
using Zenject;

public class SignalsInstaller : Installer<SignalsInstaller>
{
    public override void InstallBindings()
    {
        SignalBusInstaller.Install(Container);

        // Scene Management
        Container.DeclareSignal<SLoadingDone>();

        // Game signals
        Container.DeclareSignal<SPauseGame>();
        Container.DeclareSignal<SGameOver>();
        Container.DeclareSignal<SResumeGame>();
        Container.DeclareSignalWithInterfaces<SStartGame>().OptionalSubscriber();


        // MonsterSignal
        Container.DeclareSignal<SStartingBrewing>();
        Container.DeclareSignal<SMonsterBrewed>();
        Container.DeclareSignal<SRecipeBrewed>();

        // RecipeSignal
        Container.DeclareSignal<SNewRecipeUnlockSignal>();

        // Background Music
        Container.DeclareSignal<SBackgroundChangeMusicSignal>();
        Container.DeclareSignal<SFadeOut>();
        //Container.DeclareSignalWithInterfaces<IBackgroundStartMusicSignal>().OptionalSubscriber();
        //Container.DeclareSignalWithInterfaces<IBackgroundStopMusicSignal>().OptionalSubscriber();

        // other sfx
        Container.DeclareSignal<BackgroundStartMusicSignal>().OptionalSubscriber();
        Container.DeclareSignal<SBackgroundChangeMusicSignal>().OptionalSubscriber();
        Container.DeclareSignal<SBackgroundStopMusicSignal>().OptionalSubscriber();
        Container.DeclareSignal<SIngredientEntered>();
        Container.DeclareSignalWithInterfaces<SCldrnBoilIdle>().OptionalSubscriber();
        Container.DeclareSignalWithInterfaces<SCldrnBoilBeckon>().OptionalSubscriber();
        Container.DeclareSignalWithInterfaces<SCldrnBoilActive>().OptionalSubscriber();
        Container.DeclareSignalWithInterfaces<SCldrnBoilSummoning>().OptionalSubscriber();
        Container.DeclareSignalWithInterfaces<SIngredDropCldrnBig>().OptionalSubscriber();
        Container.DeclareSignalWithInterfaces<SIngredDropCldrnMedium>().OptionalSubscriber();
        Container.DeclareSignalWithInterfaces<SIngredDropCldrnSmall>().OptionalSubscriber();
        Container.DeclareSignalWithInterfaces<SIngredBin>().OptionalSubscriber();
        Container.DeclareSignalWithInterfaces<SIngredPickup>().OptionalSubscriber();
        Container.DeclareSignalWithInterfaces<SCustomMoveIn>().OptionalSubscriber();
        Container.DeclareSignalWithInterfaces<SCustomMoveOut>().OptionalSubscriber();
        Container.DeclareSignalWithInterfaces<SAlive>().OptionalSubscriber();


        // UI
        Container.DeclareSignal<SBookOpen>();
        Container.DeclareSignal<SBookPage>();
        Container.DeclareSignal<SBookClose>();
        Container.DeclareSignal<SClientMake>();
        Container.DeclareSignal<SClientEnter>();

        // TaskSignal
        Container.DeclareSignal<STaskLaunched>();
        Container.DeclareSignal<IPauseRequired>();
        Container.DeclareSignal<STaskEnded>();
        Container.DeclareSignal<SUpdateHiddenStat>();
    }

}
