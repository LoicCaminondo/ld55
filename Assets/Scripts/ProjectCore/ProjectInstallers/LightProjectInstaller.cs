using ProjectCore;
using Signals;
using UnityEngine.SocialPlatforms.Impl;
using Zenject;

public class LightProjectInstaller : MonoInstaller
{

    public override void InstallBindings()
    {
        SignalsInstaller.Install(Container);
        CustomUISignalInstaller.Install(Container);
        VolumeInstaller.Install(Container);
        DisplayInstaller.Install(Container);


        Container.BindInterfacesAndSelfTo<SceneManager>().AsSingle();
        Container.BindInterfacesAndSelfTo<GameSettingsManager>().AsSingle();

        Container.Bind<AsyncProcessor>().FromNewComponentOnNewGameObject().AsSingle();
        Container.BindFactory<SignalWaiter<SLoadingDone>, SignalWaiter<SLoadingDone>.Factory>().AsSingle();
    }
}