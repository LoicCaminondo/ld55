using ProjectCore;
using Sirenix.OdinInspector;
using UnityEngine;
using Zenject;

[CreateAssetMenu(fileName = "DefaultParametersInstaller", menuName = "Installers/DefaultParameters")]
public class DefaultSettingsInstaller : ScriptableObjectInstaller<DefaultSettingsInstaller>
{
    [SerializeField] private SceneManager.Settings _sceneManagerSettings;
    [SerializeField] private GameSettingsManager.Settings _gamesSettings;

    public override void InstallBindings()
    {
        Container.BindInstance(_sceneManagerSettings);
        Container.BindInstance(_gamesSettings);
    }
}