using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Raining : MonoBehaviour
{
    [SerializeField] private List<Droplet> dropletPrefabs = new List<Droplet>();
    [SerializeField] private float dropletSpawnRate = 0.1f;
    [SerializeField] private float dropletDeepest = 5.0f;
    [SerializeField] private Rect dropletArea = new Rect(-5, 5, 10, 10);
    private List<GameObject> droplets = new List<GameObject>();

    private void OnDrawGizmos()
    {
        Gizmos.color = Color.blue;
        Gizmos.DrawWireCube(new Vector3(dropletArea.x + dropletArea.width / 2, dropletArea.y + dropletArea.height / 2, 0), new Vector3(dropletArea.width, dropletArea.height, 0));
    }

    void Update()
    {
        // check if we need to spawn a new droplet
        if (Random.value < dropletSpawnRate)
        {
            //check if we can use an existing droplet
            foreach (GameObject droplet in droplets)
            {
                if (droplet.transform.position.y < dropletArea.yMin)
                {
                    droplet.transform.position = new Vector3(Random.Range(dropletArea.xMin, dropletArea.xMax), dropletArea.yMax, Random.Range(0, dropletDeepest));
                    return;
                }
            }
            Droplet drop = Instantiate(dropletPrefabs[Random.Range(0, dropletPrefabs.Count)]);
            drop.DropFrom(new Vector3(Random.Range(dropletArea.xMin, dropletArea.xMax), dropletArea.yMax, Random.Range(0, dropletDeepest)));
            droplets.Add(drop.gameObject);
        }
    }
}
