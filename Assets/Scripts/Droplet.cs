
using UnityEngine;

public class Droplet : MonoBehaviour
{
    [SerializeField] private SpriteRenderer spriteRenderer;
    [SerializeField] float speed = 5.0f;

    private float rotationSpeed = 0.0f;

    public void DropFrom(Vector3 position)
    {
        transform.position = position;
        transform.localScale = Vector3.one / (position.z * 10 + 1);
        spriteRenderer.sortingOrder = (int)position.z * -1000;
    }

    private void Start()
    {
        spriteRenderer = GetComponent<SpriteRenderer>();
        rotationSpeed = Random.Range(-1.0f, 1.0f) * 90;
    }

    void Update()
    {
        transform.position += Vector3.down * (speed / (transform.position.z * 5 + 1)) * Time.deltaTime;
        transform.Rotate(Vector3.forward, rotationSpeed * Time.deltaTime);
    }
}
