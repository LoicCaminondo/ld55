using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Cysharp.Threading.Tasks;

public class DelayComponentActivator : MonoBehaviour
{
    [SerializeField] MonoBehaviour componentToActivate;
    [SerializeField] int delayMs = 100;

    void Awake()
    {
        componentToActivate.enabled = false;
    }

    async void Start()
    {
        await UniTask.Delay(delayMs);
        componentToActivate.enabled = true;
    }
}
