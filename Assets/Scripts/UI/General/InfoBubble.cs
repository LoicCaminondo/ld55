using MoreMountains.Feedbacks;
using Sirenix.OdinInspector;
using System.Collections;
using System.Collections.Generic;
using TMPro;
#if UNITY_EDITOR
using UnityEditor.SceneManagement;
#endif
using UnityEditor;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.UIElements;
using Zenject;
using Image = UnityEngine.UI.Image;

public class InfoBubble : MonoBehaviour
{
    [Inject] SignalBus _signalBus;
    [Inject(Id = "Appear")] MMF_Player _appearPlayer;
    [Inject(Id = "Disappear")] MMF_Player _disappearPlayer;
    [Inject] TMP_Text _textObj;
    [Inject] Image _background;



    public TMP_Text TextObj => _textObj;

    private bool _isOn = false;
    private bool _isEnabled = false;


    private void Awake()
    {
        //_signalBus.Subscribe<SIngredPickup>(Disable);
    }

    public void Enable()
    {
        _isEnabled = true;
    }

    public void Disable()
    {
        OnMouseExit();
        _isEnabled = false;

    }

    public void OnMouseOver()
    {
        if (!_isEnabled)
            return;
        if (!_isOn)
        {
            _isOn = true;
            //Resize();
            _appearPlayer.StopFeedbacks();
            _disappearPlayer.StopFeedbacks();
            _appearPlayer.PlayFeedbacks();
        }
    }

    public void OnMouseExit()
    {
        if (!_isEnabled)
            return;
        if (_isOn)
        {
            _isOn = false;
            _appearPlayer.StopFeedbacks();
            _disappearPlayer.StopFeedbacks();
            _disappearPlayer.PlayFeedbacks();

        }
    }

    [Button("Resize")]
    public void Resize()
    {
        TMP_Text textObj = _textObj;
        Image background = _background;

        if (_textObj == null || _background == null)
        {
            textObj = transform.Find("Content").GetComponentInChildren<TMP_Text>();
            background = transform.Find("Content").Find("Background").GetComponent<Image>();
        }

        //textObj.transform.localPosition = Vector3.zero;
        //background.transform.localPosition = Vector3.zero;
        textObj.rectTransform.anchoredPosition = Vector2.zero;
        background.rectTransform.anchoredPosition = Vector2.zero;

        float preferredHeight = textObj.preferredHeight;
        float currentHeight = textObj.GetPixelAdjustedRect().size.y;
        float ratio = preferredHeight / currentHeight;
        textObj.rectTransform.sizeDelta = new Vector2(textObj.rectTransform.sizeDelta.x, textObj.rectTransform.sizeDelta.y * ratio);
        background.rectTransform.sizeDelta = new Vector2(background.rectTransform.sizeDelta.x, background.rectTransform.sizeDelta.y * ratio);

#if UNITY_EDITOR
        if (!Application.isPlaying)
        {
            EditorUtility.SetDirty(textObj);
            EditorUtility.SetDirty(textObj.rectTransform);
            EditorUtility.SetDirty(background.rectTransform);
            EditorUtility.SetDirty(background);
            EditorUtility.SetDirty(gameObject);
            EditorSceneManager.MarkSceneDirty(EditorSceneManager.GetActiveScene());
        }

#endif
    }
}

