
using UnityEngine;
using Zenject;

public class Notif : MonoBehaviour
{
    [Inject(Id = "Notif")] SpriteRenderer _notifSprite;

    private void Awake()
    {
        _notifSprite.gameObject.SetActive(false);
    }

    public void ActiveNotif()
    {
        _notifSprite.gameObject.SetActive(true);
    }

    public void DeactiveNotif()
    {
        _notifSprite.gameObject.SetActive(false);
    }

}

