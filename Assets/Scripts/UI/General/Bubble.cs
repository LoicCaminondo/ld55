using System.Collections;
using System.Collections.Generic;
using Febucci.UI;
using TMPro;
using UnityEngine;
using Cysharp.Threading.Tasks;
using System.Threading;

public class Bubble : MonoBehaviour
{
    [SerializeField] private TypewriterByCharacter _clientBubbleTypewriter;
    [SerializeField] private float _waitTime = 3f;

    public async UniTask Display(string text)
    {
        gameObject.SetActive(true);
        _clientBubbleTypewriter.ShowText(text);

        await UniTask.WhenAny(
            _clientBubbleTypewriter.onTextShowed.GetAsyncEventHandler(this.GetCancellationTokenOnDestroy()).OnInvokeAsync(),
                                        UniTask.WaitUntil(() => Input.GetMouseButtonDown(0), cancellationToken: this.GetCancellationTokenOnDestroy())
                                        );

        _clientBubbleTypewriter.SkipTypewriter();
        List<UniTask> tasks = new List<UniTask>();
        tasks.Add(UniTask.WaitUntil(() => Input.GetMouseButtonDown(0), cancellationToken: this.GetCancellationTokenOnDestroy()));
        if (_waitTime > 0)
        {
            tasks.Add(UniTask.WaitForSeconds(_waitTime, cancellationToken: this.GetCancellationTokenOnDestroy()));
        }
        await UniTask.WhenAny(tasks);
    }
}
