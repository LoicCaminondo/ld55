using MoreMountains.Feedbacks;
using System.Collections;
using System.Collections.Generic;
using Unity.Collections.LowLevel.Unsafe;
using UnityEngine;
using UnityEngine.UI;
using Zenject;

[RequireComponent(typeof(Slider))]
public class SliderVolumes : MonoBehaviour
{
    [Inject] readonly  VolumeManager _volumeManager;

    [SerializeField] VolumeBus _busType;
    [SerializeField] MMF_Player _feedback;

    public void Start()
    {
        Slider slider = GetComponent<Slider>();

        switch (_busType)
        {
            case VolumeBus.Sound:
                slider.value = _volumeManager.SoundVolume;
                break;
            case VolumeBus.Music:
                slider.value = _volumeManager.MusicVolume;
                break;
        }
    }

    public void SliderValueChanged(float newValue)
    {
        switch (_busType)
        {
            case VolumeBus.Sound:
                _volumeManager.SoundVolume = newValue;
                break;
            case VolumeBus.Music:
                _volumeManager.MusicVolume = newValue;
                break;
        }
        _feedback.PlayFeedbacks();
    }


    internal enum VolumeBus
    {
        Sound,
        Music
    }
}
