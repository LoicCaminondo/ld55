using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Zenject;

public class DisplayInstaller : Installer<DisplayInstaller>
{
    public override void InstallBindings()
    {
        Container.BindInterfacesAndSelfTo<DisplayManager>().AsSingle().NonLazy();
    }
}

public class DisplayManager : IInitializable
{
    private int _displayIndex;

    public List<DisplayInfo> DisplayInfos { get; private set; }
    public int ScreenIndex => _displayIndex;

    public FullScreenMode ScreenMode => Screen.fullScreenMode;

    public void Initialize()
    {
        DisplayInfos = new List<DisplayInfo>();
    }


    public void SetWindowed()
    {
        Screen.fullScreenMode = FullScreenMode.Windowed;
    }

    public void SetBorderless()
    {

        Screen.fullScreenMode = FullScreenMode.FullScreenWindow;

    }
    public void SetFullscreen()
    {
        Screen.fullScreenMode = FullScreenMode.ExclusiveFullScreen;
    }
}
