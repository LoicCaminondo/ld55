using System;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using Unity.VisualScripting;
using UnityEngine;
using UnityEngine.InputSystem;
using UnityEngine.UI;
using Zenject;

public class OptionPanel : MonoBehaviour
{
    [Inject] readonly DisplayManager _displayManager;

    public void Start()
    {
        switch (_displayManager.ScreenMode)
        {
            case FullScreenMode.ExclusiveFullScreen:
                SetFullscreen(false);
                break;
            case FullScreenMode.FullScreenWindow:
                SetBorderless(false);
                break;
            case FullScreenMode.Windowed:
                SetWindowed(false);
                break;
            default:
                SetWindowed(false);
                break;
        }
    }

    #region DisplayMode

    [Inject(Id = "DisplayText")] TMP_Text _displayText;
    [Inject(Id = "LeftDisplayType")] Button _leftDisplayButton;
    [Inject(Id = "RightDisplayType")] Button _rightDisplayButton;


    private void SetWindowed(bool setDisplay)
    {
        if (setDisplay)
            _displayManager.SetWindowed();
        _displayText.text = "Windowed";
        _rightDisplayButton.gameObject.SetActive(false);
    }

    private void SetBorderless(bool setDisplay)
    {

        if (setDisplay)
            _displayManager.SetBorderless();
        _rightDisplayButton.gameObject.SetActive(true);
        _leftDisplayButton.gameObject.SetActive(true);
        _displayText.text = "Borderless";

    }
    private void SetFullscreen(bool setDisplay)
    {
        if (setDisplay)
            _displayManager.SetFullscreen();
        _displayText.text = "Fullscreen";
        _leftDisplayButton.gameObject.SetActive(false);
    }


    public void ClickLeftDisplay()
    {

        if (Screen.fullScreenMode == FullScreenMode.FullScreenWindow)
        {
            SetFullscreen(true);

        }
        else if (Screen.fullScreenMode == FullScreenMode.Windowed)
            SetBorderless(true);

    }
    public void ClickRightDisplay()
    {

        if (Screen.fullScreenMode == FullScreenMode.ExclusiveFullScreen)
        {
            SetBorderless(true);
        }
        else if (Screen.fullScreenMode == FullScreenMode.FullScreenWindow)
        {
            SetWindowed(true);

        }

    }

    #endregion


}
