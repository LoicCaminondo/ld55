using System;
using System.Collections;
using System.Collections.Generic;
using ProjectCore;
using Signals;
using UnityEngine;
using UnityEngine.InputSystem;
using Zenject;

public class PauseScreen : MonoBehaviour
{
    [Inject] readonly SignalBus _signalBus;
    [Inject] readonly RectTransform _panel;
    [Inject(Optional = true)] readonly AnimationPauseScreen _animationPauseScreen;

    Inputs _inputs;

    private GameState _pauseState = GameState.Run;

    void Start()
    {
        _inputs = new Inputs();
        _inputs.UI.Pause.performed += OnPause;
        _signalBus.Subscribe<SLoadingDone>(OnLoadingDone);
        _signalBus.Subscribe<SGameOver>(OnGameOver);
        _signalBus.Subscribe<IPauseRequired>(OnPauseRequired);
        _panel.gameObject.SetActive(false);
        _pauseState = GameState.Run;
    }


    private void OnDestroy()
    {
        _inputs.Disable();
        _signalBus.TryUnsubscribe<SLoadingDone>(OnLoadingDone);
        _signalBus.TryUnsubscribe<SGameOver>(OnGameOver);
        _signalBus.TryUnsubscribe<IPauseRequired>(OnPauseRequired);
    }

    private void OnPauseRequired()
    {
        if (_pauseState == GameState.Run)
        {
            PauseGame();
        }
    }

    private void OnPause(InputAction.CallbackContext context)
    {
        if (_pauseState == GameState.Pause)
        {
            ResumeGame();
        }
        else if (_pauseState == GameState.Run)
        {
            PauseGame();
        }
        else if (_pauseState == GameState.Option)
        {
            CloseOptions();
        }
    }

    public void ResumeGame()
    {
        _signalBus.Fire<SResumeGame>();
        _pauseState = GameState.Run;
        //gameObject.SetActive(false);
        if (_animationPauseScreen)
            _animationPauseScreen.TranslateOut();
        else
            _panel.gameObject.SetActive(false);
        Time.timeScale = 1f;
    }


    private void PauseGame()
    {
        _signalBus.Fire<SPauseGame>();
        _pauseState = GameState.Pause;
        if (_animationPauseScreen)
            _animationPauseScreen.TranslateIn();
        else
            _panel.gameObject.SetActive(true);
        Time.timeScale = 0f;
    }

    public void OpenOptions()
    {
        if (_pauseState == GameState.Pause)
        {
            _pauseState = GameState.Option;
            if (_animationPauseScreen)
                _animationPauseScreen.OptionIn();
        }

    }

    public void CloseOptions()
    {
        _pauseState = GameState.Pause;
        if (_animationPauseScreen)
            _animationPauseScreen.OptionOut();
    }


    private void OnLoadingDone()
    {
        _inputs.Enable();
    }

    private void OnGameOver()
    {
        _inputs.Disable();
    }

    internal enum GameState
    {
        Run,
        Pause,
        Option
    }
}
