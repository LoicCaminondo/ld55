using MoreMountains.Feedbacks;
using Signals;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Zenject;

public class AnimationPauseScreen : MonoBehaviour
{
    [Inject(Id = "In")] readonly MMF_Player _translateIn;
    [Inject(Id = "Out")] readonly MMF_Player _translateOut;
    [Inject(Id = "OptionIn")] readonly MMF_Player _optionIn;
    [Inject(Id = "OptionOut")] readonly MMF_Player _optionOut;


    public void TranslateIn()
    {
        _translateOut.StopFeedbacks();
        _translateIn.PlayFeedbacks();
    }

    public void TranslateOut()
    {
        _translateIn.StopFeedbacks();
        _translateOut.PlayFeedbacks();
    }

    public void OptionIn()
    {
        _optionIn.PlayFeedbacks();
    }

    public void OptionOut()
    {
        _optionIn.StopFeedbacks();
        _optionOut.PlayFeedbacks();
    }

}
