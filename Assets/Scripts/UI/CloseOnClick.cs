using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Zenject;

public class CloseOnClick : MonoBehaviour
{
    [Inject] SignalBus _signalBus;

    public GameObject toClose;

    private void OnMouseDown()
    {
        toClose.SetActive(false);
        _signalBus.Fire<SimpleTaskManager.SGameLaunch>();
    }
}
